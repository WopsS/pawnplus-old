﻿namespace PawnPlus
{
    partial class Vehicles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VehicleWorker = new System.ComponentModel.BackgroundWorker();
            this.VehicleListView = new System.Windows.Forms.ListView();
            this.Column = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LoadingLabel = new System.Windows.Forms.Label();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // VehicleWorker
            // 
            this.VehicleWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.VehicleWorker_DoWork);
            this.VehicleWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.VehicleWorker_RunWorkerCompleted);
            // 
            // VehicleListView
            // 
            this.VehicleListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.VehicleListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.VehicleListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Column});
            this.VehicleListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VehicleListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.VehicleListView.Location = new System.Drawing.Point(0, 0);
            this.VehicleListView.Name = "VehicleListView";
            this.VehicleListView.Size = new System.Drawing.Size(325, 435);
            this.VehicleListView.TabIndex = 1;
            this.VehicleListView.UseCompatibleStateImageBehavior = false;
            this.VehicleListView.View = System.Windows.Forms.View.Details;
            this.VehicleListView.Resize += new System.EventHandler(this.VehicleListView_Resize);
            // 
            // Column
            // 
            this.Column.Text = "Column";
            this.Column.Width = 303;
            // 
            // LoadingLabel
            // 
            this.LoadingLabel.BackColor = System.Drawing.Color.Transparent;
            this.LoadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadingLabel.Font = new System.Drawing.Font("CordiaUPC", 50F);
            this.LoadingLabel.Location = new System.Drawing.Point(0, 0);
            this.LoadingLabel.Name = "LoadingLabel";
            this.LoadingLabel.Size = new System.Drawing.Size(325, 435);
            this.LoadingLabel.TabIndex = 2;
            this.LoadingLabel.Text = "Loading...";
            this.LoadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // Vehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 435);
            this.Controls.Add(this.VehicleListView);
            this.Controls.Add(this.LoadingLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Vehicles";
            this.Text = "Vehicles";
            this.Load += new System.EventHandler(this.Vehicles_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker VehicleWorker;
        public System.Windows.Forms.ListView VehicleListView;
        private System.Windows.Forms.Label LoadingLabel;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ColumnHeader Column;

    }
}