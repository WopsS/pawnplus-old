﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PawnPlus
{
    public class ConfigFiles
    {
        public static string SettingName = null;
        public static string[] SettingsSplited;
        public static string User = null;
        public static string Theme = null;
        public static string Language = null;
        public static string CompilerName = null;
        public static string CompilerArgs = null;

        public static bool ThemeChanged = false;
        public static bool LanguageChanged = false;

        public static void CreateDefaultFiles()
        {
            if (!File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "Settings.ini")))
            {
                string DefaultSettings = "User=" + Environment.UserName + "\r\n" + "Theme=White\r\n" + "Language=English\r\n" + "CompilerName=pawncc.exe\r\n" + "CompilerParameters=";

                File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "Settings.ini"), DefaultSettings);
            }
        }

        public static void ReadConfigFile()
        {
            StreamReader SettingsFileReader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Settings.ini");

            while (!SettingsFileReader.EndOfStream)
            {
                SettingName = SettingsFileReader.ReadLine();

                SettingsSplited = SettingName.Split('=');

                if (SettingsSplited[0] == "User")
                    User = SettingsSplited[1];

                if (SettingsSplited[0] == "Theme")
                    Theme = SettingsSplited[1];

                if (SettingsSplited[0] == "Language")
                    Language = SettingsSplited[1];

                if (SettingsSplited[0] == "CompilerName")
                    CompilerName = SettingsSplited[1];

                if (SettingsSplited[0] == "CompilerParameters")
                    CompilerArgs = SettingsSplited[1];
            }
            SettingsFileReader.Close();

            if (Theme == "Black" && Language == "Spanish")
                Theme = "Negro";

            if (Theme == "White" && Language == "Spanish")
                Theme = "Blanco";

            if (Theme == "Black" && Language == "Romanian")
                Theme = "Negru";

            if (Theme == "White" && Language == "Romanian")
                Theme = "Alb";
        }

        public static void SaveConfigFile()
        {
            if (Theme == "Negro" && Language == "Spanish")
                Theme = "Black";

            if (Theme == "Blanco" && Language == "Spanish")
                Theme = "White";

            if (Theme == "Negru" && Language == "Romanian")
                Theme = "Black";

            if (Theme == "Alb" && Language == "Romanian")
                Theme = "White";

            File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "Settings.ini"));

            string DefaultSettings = "User=" + User + "\r\n" + "Theme=" + Theme + "\r\n" + "Language=" + Language + "\r\n" +"CompilerName=" + CompilerName + "\r\n" + "CompilerParameters=" + CompilerArgs;

            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "Settings.ini"), DefaultSettings);
        }
    }

    class InvokeListView
    {
        delegate void UniversalVoidDelegate();

        public static void ControlInvike(Control control, Action function)
        {
            if (control.IsDisposed || control.Disposing)
                return;

            if (control.InvokeRequired)
            {
                control.Invoke(new UniversalVoidDelegate(() => ControlInvike(control, function)));
                return;
            }
            function();
        }
    }
}
