﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;

namespace PawnPlus
{
    public partial class Downloading : Form
    {
        Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

        public Downloading()
        {
            InitializeComponent();
        }

        private void Downloading_Load(object sender, EventArgs e)
        {
            if(ConfigFiles.Theme == "Black")
            {
                this.BackColor = Color.FromArgb(45, 45, 48);
                this.ForeColor = Color.White;
                label1.ForeColor = Color.White;
                DownloadPercent.ForeColor = Color.White;
                MBDownloaded.ForeColor = Color.White;
            }

            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates"))
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates");

            string NewVersion = new WebClient().DownloadString("https://github.com/WopsS/PawnPlus/raw/master/version");
            NewVersion = NewVersion.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");

            WebClient webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.DonwloadProcess);
            webClient.DownloadFileAsync(new Uri("https://github.com/WopsS/PawnPlus/Versions/download/" + NewVersion + "/PawnPlus.exe"), Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates\\PawnPlus.exe");
        }

        public void DonwloadProcess(object sender, DownloadProgressChangedEventArgs e)
        {
            this.BringToFront();

            MBDownloaded.Text = string.Format("{0} MB's / {1} MB's", (e.BytesReceived / 1024d / 1024d).ToString("0.00"), (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));

            DonwloadProcessBar.Value = e.ProgressPercentage;
            DownloadPercent.Text = e.ProgressPercentage.ToString() + "%";
        }
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates\\PawnPlus.exe");
            Application.Exit();
        }
        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romanian")
            {
                this.Text = "Descărcare";
                label1.Text = "Descarc actualizarea...";
            }
            if (ConfigFiles.Language == "Spanish")
            {
                this.Text = "Descargando";
                label1.Text = "Descargando actualización...";
            }
        }
    }
}
