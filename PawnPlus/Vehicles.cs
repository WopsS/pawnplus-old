﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace PawnPlus
{
    public partial class Vehicles : DockContent
    {
        string[] VehicleName = {"Landstalker", "Bravura", "Buffalo", "Linerunner", "Perenniel", "Sentinel", "Dumper", "Firetruck", "Trashmaster", "Stretch", "Manana", "Infernus", "Voodoo", "Pony", "Mule", "Cheetah", 
                                   "Ambulance", "Leviathan", "Moonbeam", "Esperanto", "Taxi", "Washington", "Bobcat", "Mr Whoopee", "BF Injection", "Hunter", "Premier", "Enforcer", "Securicar", "Banshee", "Predator", "Bus", 
                                   "Rhino", "Barracks", "Hotknife", "Article Trailer", "Previon", "Coach", "Cabbie", "Stallion", "Rumpo", "RC Bandit", "Romero", "Packer", "Monster", "Admiral", "Squallo", "Seasparrow", 
                                   "Pizzaboy", "Tram", "Article Trailer 2", "Turismo", "Speeder", "Reefer", "Tropic", "Flatbed", "Yankee", "Caddy", "Solair", "Topfun Van (Berkley's RC)", "Skimmer", "PCJ-600", "Faggio", 
                                   "Freeway", "RC Baron", "RC Raider", "Glendale", "Oceanic", "Sanchez", "Sparrow", "Patriot", "Quad", "Coastguard", "Dinghy", "Hermes", "Sabre", "Rustler", "ZR-350", "Walton", "Regina", 
                                   "Comet", "BMX", "Burrito", "Camper", "Marquis", "Baggage", "Dozer", "Maverick", "SAN News Maverick", "Rancher", "FBI Rancher", "Virgo", "Greenwood", "Jetmax", "Hotring Racer", "Sandking", 
                                   "Blista Compact", "Police Maverick", "Boxville", "Benson", " Mesa", "RC Goblin", "Hotring Racer", "Hotring Racer", "Bloodring Banger", "Rancher", "Super GT", "Elegant", "Journey", "Bike", 
                                   "Mountain Bike", "Beagle", "Cropduster", "Stuntplane", "Tanker", "Roadtrain", "Nebula", "Majestic", "Buccaneer", "Shamal", "Hydra", "FCR-900", "NRG-500", "HPV1000", "Cement Truck", 
                                   "Towtruck", "Fortune", "Cadrona", "FBI Truck", "Willard", "Forklift", "Tractor", "Combine Harvester", "Feltzer", "Remington", "Slamvan", "Blade", "Freight (Train)", "Brownstreak (Train)", 
                                   "Vortex", "Vincent", "Bullet", "Clover", "Sadler", "Firetruck LA", "Hustler", "Intruder", "Primo", "Cargobob", "Tampa", "Sunrise", "Merit", "Utility Van", "Nevada", "Yosemite", "Windsor", 
                                   "Monster \"A\"", "Monster \"B\"", "Uranus", "Jester", "Sultan", "Stratum", "Elegy", "Raindance", "RC Tiger", "Flash", "Tahoma", "Savanna", "Bandito", "Freight Flat Trailer (Train)", 
                                   "Streak Trailer (Train)", "Kart", "Mower", "Dune", "Sweeper", "Broadway", "Tornado", "AT400", "DFT-30", "Huntley", "Stafford", "BF-400", "Newsvan", "Tug", "Petrol Trailer", "Emperor", 
                                   "Wayfarer", "Euros", "Hotdog", "Club", "Freight Box Trailer (Train)", "Article Trailer 3", "Andromada", "Dodo", "RC Cam", "Launch", "Police Car (LSPD)", "Police Car (SFPD)", 
                                   "Police Car (LVPD)", "Police Ranger", "Picador", "S.W.A.T.", "Alpha", "Phoenix", "Glendale Shit", "Sadler Shit", "Baggage Trailer \"A\"", "Baggage Trailer \"B\"", "Tug Stairs Trailer", 
                                   "Boxville", "Farm Trailer", "Utility Trailer" 
                               };

        public Vehicles()
        {
            InitializeComponent();
        }

        private void Vehicles_Load(object sender, EventArgs e)
        {
            TranslateToLanguage();
            SetColumnSize(VehicleListView);
            VehicleListView.Visible = false;
            VehicleWorker.RunWorkerAsync();
        }

        private void VehicleListView_Resize(object sender, EventArgs e)
        {
            try
            {
                SetColumnSize((ListView)sender);
            }
            catch { }
        }

        private void SetColumnSize(ListView listview)
        {
            listview.Columns[listview.Columns.Count - 1].Width = -1;
        }

        private void VehicleWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            imageList.ImageSize = new Size(204, 125);

            int d = 0;

            for (int i = 400; i <= 611; i++)
            {
                imageList.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory + "\\Vehicles", "Vehicle_" + i + ".png")));
                InvokeListView.ControlInvike(VehicleListView, () => VehicleListView.Items.Add(VehicleName[d] + " ID " + i, d));
                d++;
            }

            InvokeListView.ControlInvike(VehicleListView, () => VehicleListView.LargeImageList = imageList);
            InvokeListView.ControlInvike(VehicleListView, () => VehicleListView.View = View.LargeIcon);
        }

        private void VehicleWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadingLabel.Visible = false;
            InvokeListView.ControlInvike(VehicleListView, () => VehicleListView.Visible = true);
        }

        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romana")
            {
                this.Text = "Masini";
                LoadingLabel.Text = "Încărcare";
            }
        }
    }
}
