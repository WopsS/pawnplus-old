﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScintillaNET;
using WeifenLuo.WinFormsUI.Docking;
using System.Diagnostics;

namespace PawnPlus
{
    public partial class TextEditor : DockContent
    {
        public int GoToLine;
        public bool IsSaved = true; // This will check the user save .pwn before exit.
  
        public TextEditor()
        {
            InitializeComponent();
        }

        private void TextEditor_Load(object sender, EventArgs e)
        {
            // Show line numbers. 60 allow large line numbers. DON'T CHANGE IT!
            //TextBox.Margins[0].Width = 60;
            /*int Last_Line = 0;

            TextBox.TextChanged += (o, s) =>
            {
                int Lines = TextBox.Lines.Count;
                if (Lines != Last_Line)
                {
                    Last_Line = Lines;
                    TextBox.Margins[0].Width = TextRenderer.MeasureText(Lines.ToString(), TextBox.Font).Width + 10;
                }
            };*/
            TextBox.AutoComplete.SingleLineAccept = false;
            TextBox.Lexing.Keywords[0] = "if else for new int float bool break switch case default return public forward stock enum"; 
        }

        private void TextBox_SelectionChanged(object sender, EventArgs e)
        {
            // Make box rounded.
            TextBox.Indicators[1].Style = IndicatorStyle.RoundBox;
            // Color of Box.
            TextBox.Indicators[1].Color = System.Drawing.Color.FromArgb(56, 124, 125); // Hex color #387c7d

            var TextRange = TextBox.GetRange(0, TextBox.Text.Length - 1);
            TextRange.ClearIndicator(1);

            if (TextBox.Selection.Start != TextBox.Selection.End)
            {
                string selectedWord = TextBox.Selection.Text;
                TextBox.FindReplace.Flags = SearchFlags.WholeWord;
                IList<Range> RageForIndicator = TextBox.FindReplace.FindAll(selectedWord);
                foreach (var r in RageForIndicator)
                    r.SetIndicator(1);
            }
        }

        private void TextBox_CharAdded(object sender, CharAddedEventArgs e)
        {
            // Add autocomplet list. Don't change nothing here.
            if (Char.IsLetter(e.Ch))
            {
                string Word = TextBox.GetWordFromPosition(TextBox.NativeInterface.GetCurrentPos());

                if (String.IsNullOrEmpty(Word))
                    return;

                List<string> PossibleItems = null;

                PossibleItems = new List<string>();

                foreach (string item in (Include.AutoCompletItems.FindAll(items => items.StartsWith(Word, StringComparison.OrdinalIgnoreCase))))
                    PossibleItems.Add(item);
                
                if (PossibleItems.Count > 0)
                {
                    PossibleItems.Sort();
                    TextBox.AutoComplete.DropRestOfWord = false;
                    TextBox.AutoComplete.List.Clear();
                    TextBox.AutoComplete.List = PossibleItems;
                    TextBox.AutoComplete.Show();
                }

            }
            // Add indention.
            if (e.Ch == '\r')
            {
                Line current = TextBox.Lines.Current;
                current.Indentation = current.Previous.Indentation;
                TextBox.CurrentPos = current.IndentPosition;
            }
            // Add brace and space.
            if (e.Ch == '{')
            {
                string CurrentText = TextBox.Lines.Current.Text;
                int Pressed = 0;

                foreach (char charText in CurrentText.ToCharArray())
                {
                    if (charText == '\t')
                        Pressed++;
                    else 
                        break;
                }

                TextBox.InsertText("\n");

                for (int i = 0; i != Pressed + 1; i++)
                    TextBox.InsertText("\t");

                int CurrentPosition = TextBox.CurrentPos;

                TextBox.InsertText("\n");

                for (int i = 0; i != Pressed; i++)
                    TextBox.InsertText("\t");

                TextBox.InsertText("}");
                TextBox.CurrentPos = CurrentPosition;
            }
        }

        private void TextBox_TextDeleted(object sender, TextModifiedEventArgs e)
        {
            IsSaved = false;
        }

        private void TextBox_TextInserted(object sender, TextModifiedEventArgs e)
        {
            IsSaved = false;
        }
        public void test()
        {
            TextBox.Select();
            TextBox.Focus();
            MessageBox.Show(TextBox.Focused.ToString());

            TextBox.GoTo.Line(2); int nowPosition = TextBox.Caret.Position; int wantedPosition = nowPosition + 5; TextBox.GoTo.Position(wantedPosition); TextBox.Select();
        }
    }
}