﻿namespace PawnPlus
{
    partial class Weapons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Weapons));
            this.LoadingLabel = new System.Windows.Forms.Label();
            this.WeaponsWorker = new System.ComponentModel.BackgroundWorker();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.WeaponListView = new System.Windows.Forms.ListView();
            this.Column = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // LoadingLabel
            // 
            this.LoadingLabel.BackColor = System.Drawing.Color.Transparent;
            this.LoadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadingLabel.Font = new System.Drawing.Font("CordiaUPC", 50F);
            this.LoadingLabel.Location = new System.Drawing.Point(0, 0);
            this.LoadingLabel.Name = "LoadingLabel";
            this.LoadingLabel.Size = new System.Drawing.Size(309, 397);
            this.LoadingLabel.TabIndex = 2;
            this.LoadingLabel.Text = "Loading...";
            this.LoadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WeaponsWorker
            // 
            this.WeaponsWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WeaponsWorker_DoWork);
            this.WeaponsWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WeaponsWorker_RunWorkerCompleted);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(55, 100);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // WeaponListView
            // 
            this.WeaponListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.WeaponListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WeaponListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Column});
            this.WeaponListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WeaponListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.WeaponListView.Location = new System.Drawing.Point(0, 0);
            this.WeaponListView.Name = "WeaponListView";
            this.WeaponListView.Size = new System.Drawing.Size(309, 397);
            this.WeaponListView.TabIndex = 3;
            this.WeaponListView.UseCompatibleStateImageBehavior = false;
            this.WeaponListView.View = System.Windows.Forms.View.Details;
            // 
            // Column
            // 
            this.Column.Text = "Column";
            this.Column.Width = 303;
            // 
            // Weapons
            // 
            this.AutoHidePortion = 0.15D;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 397);
            this.Controls.Add(this.WeaponListView);
            this.Controls.Add(this.LoadingLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Weapons";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weapons";
            this.Load += new System.EventHandler(this.Weapons_Load);
            this.Resize += new System.EventHandler(this.Weapons_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LoadingLabel;
        private System.ComponentModel.BackgroundWorker WeaponsWorker;
        private System.Windows.Forms.ImageList imageList;
        public System.Windows.Forms.ListView WeaponListView;
        private System.Windows.Forms.ColumnHeader Column;
    }
}