﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using WeifenLuo.WinFormsUI.Docking;
using System.Xml;

namespace PawnPlus
{
    // De facut: sa faci inca o tema pe negru (mai ai aia de jos, alea cu #else ca apare cu alb), sa faci cloud.
    public partial class Main : Form
    {
        TextEditor texteditor = new TextEditor();
        Output output = new Output();
        Skins skins = new Skins();
        Weapons weapons = new Weapons();
        Vehicles vehicles = new Vehicles();

        Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

        string FilePath = null;
        string IDEPath = Environment.CurrentDirectory.ToString();
        string ErrorMessage = null;

        string SOutput = null;
        string Errors = null;
        string TextForCompiling = null;

        bool OpenLastFile = true;
        bool CompilerWorking = false;
        bool StatutsLabelCanBeChanged = false;

        // Layout save made by DockPanel suite team!
        DeserializeDockContent DockContentLayout;
        bool SaveLayout = true;

        #region TranslateStrings
        string OpeningNewFileString = null;
        string OpeningFileString = null;
        string ClosingFileString = null;
        string OpenedFileString = null;
        string SavedFileString = null;
        string ClosedFileString = null;
        string CompiledFinishText = null;
        string ErrorsWhenCompiling = null;
        string NewVersionAvailableString = null;
        string CurrentVersionString = null;
        string SavingFileString = null;
        string SaveChangesTitleString = null;
        string SaveChangesString = null;
        string CantCompile = null;
        string ReadyString = null;
        string CompilingString = null;
        string LineString = null;
        string ColumnString = null;
        #endregion

        public Main()
        {
            InitializeComponent();

            DockContentLayout = new DeserializeDockContent(GetLayout);

            ConfigFiles.CreateDefaultFiles();

            ConfigFiles.ReadConfigFile();

            TranslateToLanguage();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            string ThemeFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "PawnPlus.xml");

            if (!File.Exists(ThemeFile))
            {
                texteditor.Show(dockPanel, DockState.Document);
                output.Show(dockPanel, DockState.DockBottom);
                skins.Show(dockPanel, DockState.DockLeft);
                vehicles.Show(dockPanel, DockState.DockLeft);
                weapons.Show(dockPanel, DockState.DockLeft);
            }

            if (File.Exists(ThemeFile))
                dockPanel.LoadFromXml(ThemeFile, DockContentLayout);

            

            if (Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates")))
                Directory.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus\\Updates"), true);

            SetTheme();

            UpdateChecker.RunWorkerAsync();

            StatutsLabel.Text = ReadyString;

            texteditor.TextBox.IsReadOnly = true;
            texteditor.TextBox.Caret.Width = 0;

            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt")))
            {
                string ToRead = File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt"));
                Include.OpenedFile = ToRead;
                Include.PawnoFile = Directory.GetParent(Directory.GetParent(Include.OpenedFile).ToString()).ToString() + "\\pawno\\include\\";
                texteditor.TextBox.IsReadOnly = false;
                texteditor.TextBox.Caret.Width = 1;

                if (File.Exists(ToRead))
                {
                    texteditor.TextBox.Text = File.ReadAllText(ToRead, Encoding.Default);

                    texteditor.IsSaved = true;

                    FilePath = Path.GetDirectoryName(Include.OpenedFile);

                    // Initializing Functions.
                    Include.ScriptingFunctions();
                }
                else
                    File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt"));
            }
        }

        private void Main_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string ConfigFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "PawnPlus.xml");

            if (SaveLayout)
                dockPanel.SaveAsXml(ConfigFile);
            else if (File.Exists(ConfigFile))
                File.Delete(ConfigFile);

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                DialogResult AskForSave = new DialogResult();

                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();

                if (AskForSave == DialogResult.Cancel)
                    e.Cancel = true;
            }

            if (OpenLastFile == true && texteditor.TextBox.Text.Length > 0)
            {
                Include.LastFileOpened = Include.OpenedFile;
                File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt"), Include.LastFileOpened);
            }
        }

        private IDockContent GetLayout(string LayoutString)
        {
            if (LayoutString == typeof(Output).ToString())
                return output;
            if (LayoutString == typeof(Skins).ToString())
                return skins;
            if (LayoutString == typeof(Vehicles).ToString())
                return vehicles;
            if (LayoutString == typeof(Weapons).ToString())
                return weapons;
            else
                return texteditor;
        }

        private void gamemodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.IsReadOnly = false;
            texteditor.TextBox.Caret.Width = 1;

            StatutsLabel.Text = OpeningNewFileString;

            DialogResult AskForSave = new DialogResult();

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();
            }

            if (AskForSave != DialogResult.No)
            {
                texteditor.TextBox.Text = File.ReadAllText(IDEPath + "\\SAMP\\gamemodes\\Gamemode.pwn");
                texteditor.IsSaved = false;

            }

            if (AskForSave == DialogResult.Cancel) ;

            Include.ScriptingFunctions();
            StatutsLabel.Text = ReadyString;
        }

        private void filterscriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.IsReadOnly = false;
            texteditor.TextBox.Caret.Width = 1;

            StatutsLabel.Text = OpeningNewFileString;

            DialogResult AskForSave = new DialogResult();

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();
            }

            if (AskForSave != DialogResult.No)
            {
                texteditor.TextBox.Text = File.ReadAllText(IDEPath + "\\SAMP\\gamemodes\\Filterscript.pwn");
                texteditor.IsSaved = false;

            }

            if (AskForSave == DialogResult.Cancel) ;

            Include.ScriptingFunctions();
            StatutsLabel.Text = ReadyString;
        }

        private void blankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.IsReadOnly = false;
            texteditor.TextBox.Caret.Width = 1;

            StatutsLabel.Text = OpeningNewFileString;

            DialogResult AskForSave = new DialogResult();

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();
            }

            if (AskForSave != DialogResult.No)
            {
                texteditor.TextBox.Text = File.ReadAllText(IDEPath + "\\SAMP\\gamemodes\\Blank.pwn");
                texteditor.IsSaved = false;

            }

            if (AskForSave == DialogResult.Cancel) ;

            Include.ScriptingFunctions();
            StatutsLabel.Text = ReadyString;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.IsReadOnly = false;
            texteditor.TextBox.Caret.Width = 1;
 
            DialogResult AskForSave = new DialogResult();

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                {
                    StatutsLabel.Text = SavingFileString;
                    saveFileDialog.ShowDialog();
                    texteditor.IsSaved = true;
                }
            }
            if (AskForSave != DialogResult.Cancel)
            {
                StatutsLabel.Text = OpeningFileString;

                DialogResult OpenDialog = openFileDialog.ShowDialog();
                if (OpenDialog == DialogResult.OK)
                {
                    try
                    {
                        string FileForOpen = openFileDialog.FileName;

                        OpenLastFile = true;

                        output.OutBox.Clear();

                        StatutsLabel.Text = OpeningFileString;

                        texteditor.TextBox.Text = File.ReadAllText(FileForOpen, Encoding.Default);

                        StatutsLabel.Text = OpenedFileString;

                        Include.OpenedFile = FileForOpen;

                        FilePath = Path.GetDirectoryName(Include.OpenedFile);
                        Include.PawnoFile = Directory.GetParent(Directory.GetParent(Include.OpenedFile).ToString()).ToString() + "\\pawno\\include\\";
                    }
                    catch (Exception) { }
                }
            }
            Include.ScriptingFunctions();
            StatutsLabelCanBeChanged = true;
        }

        private void saveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            string ItExist = saveFileDialog.FileName;

            if (File.Exists(ItExist))
            {
                output.OutBox.Clear();

                StatutsLabel.Text = SavingFileString;

                File.WriteAllText(ItExist, texteditor.TextBox.Text, Encoding.Default);

                StatutsLabel.Text = SavedFileString;

                Include.OpenedFile = ItExist;
                FilePath = Path.GetDirectoryName(Include.OpenedFile);
            }
            else
            {
                output.OutBox.Clear();

                StatutsLabel.Text = SavingFileString;

                File.WriteAllText(ItExist, texteditor.TextBox.Text, Encoding.Default);

                StatutsLabel.Text = SavedFileString;

                Include.OpenedFile = ItExist;
                FilePath = Path.GetDirectoryName(Include.OpenedFile);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (texteditor.TextBox.TextLength > 1)
            {
                StatutsLabel.Text = SavingFileString;

                if (Include.OpenedFile != null)
                {
                    OpenLastFile = true;

                    output.OutBox.Clear();

                    StatutsLabel.Text = SavingFileString;

                    File.WriteAllText(Include.OpenedFile, texteditor.TextBox.Text, Encoding.Default);

                    StatutsLabel.Text = SavedFileString;
                }
                else
                {
                    saveFileDialog.ShowDialog();
                }

                texteditor.IsSaved = true;
            }
            StatutsLabelCanBeChanged = true;
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatutsLabel.Text = SavingFileString;

            saveFileDialog.ShowDialog();

            OpenLastFile = true;
            texteditor.IsSaved = true;
            StatutsLabel.Text = SavedFileString;
            StatutsLabelCanBeChanged = true;
        }

        private void closeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatutsLabel.Text = ClosingFileString;

            texteditor.TextBox.Text = null;

            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt")))
                File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus", "LastOpened.txt"));

            StatutsLabel.Text = ClosedFileString;
            StatutsLabelCanBeChanged = true;
            OpenLastFile = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DialogResult AskForSave = new DialogResult();

            if (texteditor.TextBox.TextLength > 0 && texteditor.IsSaved == false)
            {
                AskForSave = MessageBox.Show(SaveChangesString, SaveChangesTitleString, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();
            }

            if (AskForSave != DialogResult.Cancel)
                Application.Exit();
        }

        private void outputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (outputToolStripMenuItem.Checked == true)
            {
                outputToolStripMenuItem.Checked = false;

                output.Hide();
            }
            else
            {
                outputToolStripMenuItem.Checked = true;
                output.Show();
            }
        }

        private void skinsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (skinsToolStripMenuItem.Checked == true)
            {
                skinsToolStripMenuItem.Checked = false;

                skins.Hide();
            }
            else
            {
                skinsToolStripMenuItem.Checked = true;
                skins.Show();
            }
        }

        private void vehiclesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (vehiclesToolStripMenuItem.Checked == true)
            {
                vehiclesToolStripMenuItem.Checked = false;

                vehicles.Hide();
            }
            else
            {
                vehiclesToolStripMenuItem.Checked = true;
                vehicles.Show();
            }
        }

        private void weaponsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (weaponsToolStripMenuItem.Checked == true)
            {
                weaponsToolStripMenuItem.Checked = false;

                weapons.Hide();
            }
            else
            {
                weaponsToolStripMenuItem.Checked = true;
                weapons.Show();
            }
        }

        private void UpdateChecker_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdateAvailable updater = new UpdateAvailable();

            // Update cheacker.
            string NewVersion = new WebClient().DownloadString("https://github.com/WopsS/PawnPlus/raw/master/version");
            NewVersion = NewVersion.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");

            Version UpdateVersion = new Version(NewVersion);

            if (CurrentVersion.CompareTo(UpdateVersion) < 0)
            {
                updater.UpdateText.Text = NewVersionAvailableString + " (" + NewVersion + ").";
                updater.CurrentVersionLabel.Text = CurrentVersionString + " " + CurrentVersion.Major + "." + CurrentVersion.Minor + "." + CurrentVersion.Build;
                updater.BringToFront();
                updater.ShowDialog();
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.UndoRedo.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.UndoRedo.Redo();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.Clipboard.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (texteditor.IsActivated == false)
                output.OutBox.Copy();
            else
                texteditor.TextBox.Clipboard.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.Clipboard.Paste();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.FindReplace.ShowFind();
        }

        private void findNextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F3}");
        }

        private void findPrevToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F4}");
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.FindReplace.ShowReplace();
        }

        private void goToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texteditor.TextBox.GoTo.ShowGoToDialog();
        }

        private void compileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip1.BackColor = Color.FromArgb(202, 81, 0);
            StatutsLabel.Text = CompilingString;

            if (texteditor.TextBox.TextLength > 0)
            {
                if (Include.OpenedFile != null)
                {
                    if (CompilerWorking == false)
                    {
                        CompilerWorking = true;
                        TextForCompiling = texteditor.TextBox.Text;
                        Compiling.RunWorkerAsync();
                    }
                }
                else
                {
                    saveFileDialog.ShowDialog();
                }
            }
            else
            {
                statusStrip1.BackColor = Color.FromArgb(170, 0, 0);
                StatutsLabel.Text = CantCompile;
                StatutsLabelCanBeChanged = true;
            }
        }

        private void compileOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigFiles.ReadConfigFile();

            Options option = new Options();

            option.Show();
            option.OptionList.SetSelected(1, true);
        }

        private void Compiling_DoWork(object sender, DoWorkEventArgs e)
        {
            ConfigFiles.ReadConfigFile();

            Process Compiling = new Process();

            string TempFile = IDEPath + "\\" + Path.GetFileNameWithoutExtension(Include.OpenedFile) + ".p";
            string AmxPath = FilePath + "\\" + Path.GetFileNameWithoutExtension(Include.OpenedFile);

            StreamWriter PFile = new StreamWriter(TempFile, false, Encoding.Default);
            PFile.Write(TextForCompiling);
            PFile.Close();

            StreamWriter PwnFile = new StreamWriter(Include.OpenedFile, false, Encoding.Default);
            PwnFile.Write(TextForCompiling);
            PwnFile.Close();

            string OpenedFileParentDirectory = Directory.GetParent(Directory.GetParent(Include.OpenedFile).ToString()).ToString() + "\\pawno";

            Compiling.StartInfo.FileName = OpenedFileParentDirectory + "\\" + ConfigFiles.CompilerName;
            Compiling.StartInfo.Arguments = Path.GetFileNameWithoutExtension(Include.OpenedFile) + " -o\"" + AmxPath + "\" -;+ -(+ " + ConfigFiles.CompilerArgs;
            Compiling.StartInfo.UseShellExecute = false;
            Compiling.StartInfo.CreateNoWindow = true;
            Compiling.StartInfo.RedirectStandardError = true;
            Compiling.StartInfo.RedirectStandardOutput = true;
            Compiling.Start();

            texteditor.IsSaved = true;

            while (!Compiling.HasExited)
            {
                Errors = Compiling.StandardError.ReadToEnd();
                SOutput = Compiling.StandardOutput.ReadToEnd();
            }

            File.Delete(TempFile);

            // Thanks maddinat0r about the Regex!
            if (Errors.Contains("error") == true || Errors.Contains("warning") == true)
            {
                foreach (Match match in Regex.Matches(Errors, @"\((.+)\)\s\:\s(.+)\r\n"))
                {
                    if (match.Groups[2].ToString().Contains("undefined") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("already defined") == true)
                        ErrorMessage += "> " + match.Groups[1].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("array index out") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("invalid expression") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("expression has no effect") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("expression has") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("number of arguments") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("loose indentation") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + " at line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("symbol is never used") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("fatal error 100:") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + "." + Environment.NewLine;
                    else if (match.Groups[2].ToString().Contains("too many error") == true)
                        ErrorMessage += "> " + match.Groups[2].ToString() + Environment.NewLine;
                    else
                        ErrorMessage += "> " + match.Groups[2].ToString() + " before line " + match.Groups[1].ToString() + "." + Environment.NewLine;
                }
            }
        }

        private void Compiling_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            statusStrip1.BackColor = Color.FromArgb(0, 122, 204);
            try
            {
                output.OutBox.Clear();

                if (Errors.Length > 0)
                    output.OutBox.AppendText(ErrorMessage + Environment.NewLine);
                output.OutBox.AppendText(SOutput);
            }
            catch (Exception) { }

            CompilerWorking = false;
            ErrorMessage = null;
            if(output.OutBox.Text.Contains("error") == false)
                StatutsLabel.Text = CompiledFinishText;
            else
            {
                statusStrip1.BackColor = Color.FromArgb(170, 0, 0);
                StatutsLabel.Text = ErrorsWhenCompiling;
            }
            StatutsLabelCanBeChanged = true;
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigFiles.ReadConfigFile();

            Options option = new Options();

            option.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();

            about.Show();
        }

        public void SetTheme()
        {
            #region BlackTheme
            if (ConfigFiles.Theme != null && ConfigFiles.Theme == "Black" || ConfigFiles.Theme != null && ConfigFiles.Theme == "Negro")
            {
                this.BackColor = Color.FromArgb(45, 45, 48);

                menuStrip.Renderer = new ToolStripProfessionalRenderer(new GreyTheme());

                dockPanel.BackColor = Color.FromArgb(45, 45, 48);

                // Taked from original theme.
                #region FromOriginalTheme
                DockPanelSkin dockPaneSkin = new DockPanelSkin();

                var specialBlue = Color.FromArgb(0xFF, 0x00, 0x7A, 0xCC);
                var dot = Color.FromArgb(80, 170, 220);
                var activeTab = specialBlue;
                var mouseHoverTab = Color.FromArgb(0xFF, 28, 151, 234);
                var inactiveTab = SystemColors.Control;
                var lostFocusTab = Color.FromArgb(0xFF, 204, 206, 219);

                dockPaneSkin.AutoHideStripSkin.DockStripGradient.StartColor = specialBlue;
                dockPaneSkin.AutoHideStripSkin.DockStripGradient.EndColor = SystemColors.ControlLight;
                dockPaneSkin.AutoHideStripSkin.TabGradient.TextColor = SystemColors.ControlDarkDark;

                dockPaneSkin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = Color.FromArgb(45, 45, 48);
                dockPaneSkin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = Color.FromArgb(45, 45, 48);

                dockPaneSkin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = activeTab;
                dockPaneSkin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = Color.FromArgb(45, 45, 48);
                dockPaneSkin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.TextColor = Color.White;

                dockPaneSkin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = inactiveTab;
                dockPaneSkin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = mouseHoverTab;
                dockPaneSkin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor = Color.Black;

                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.StartColor = SystemColors.Control;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.EndColor = SystemColors.Control;

                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.StartColor = SystemColors.ControlLightLight;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.EndColor = SystemColors.ControlLightLight;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.TextColor = specialBlue;

                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.StartColor = SystemColors.Control;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.EndColor = SystemColors.Control;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.TextColor = SystemColors.GrayText;

                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.StartColor = specialBlue;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.EndColor = dot;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.TextColor = Color.White;

                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.StartColor = Color.FromArgb(45, 45, 48);
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.EndColor = SystemColors.ControlDark;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
                dockPaneSkin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.TextColor = Color.White;

                dockPanel.Skin = dockPaneSkin;
                #endregion

                #region MenuStrip
                menuStrip.BackColor = Color.FromArgb(45, 45, 48);
                menuStrip.ForeColor = Color.White;

                fileToolStripMenuItem.ForeColor = Color.White;
                newToolStripMenuItem.ForeColor = Color.White;
                gamemodeToolStripMenuItem.ForeColor = Color.White;
                filterscriptToolStripMenuItem.ForeColor = Color.White;
                blankToolStripMenuItem.ForeColor = Color.White;
                openToolStripMenuItem.ForeColor = Color.White;
                saveToolStripMenuItem.ForeColor = Color.White;
                saveAsToolStripMenuItem.ForeColor = Color.White;
                closeFileToolStripMenuItem.ForeColor = Color.White;
                exitToolStripMenuItem.ForeColor = Color.White;

                undoToolStripMenuItem.ForeColor = Color.White;
                redoToolStripMenuItem.ForeColor = Color.White;
                cutToolStripMenuItem.ForeColor = Color.White;
                copyToolStripMenuItem.ForeColor = Color.White;
                pasteToolStripMenuItem.ForeColor = Color.White;
                findToolStripMenuItem.ForeColor = Color.White;
                findNextToolStripMenuItem.ForeColor = Color.White;
                findPrevToolStripMenuItem.ForeColor = Color.White;
                replaceToolStripMenuItem.ForeColor = Color.White;
                goToToolStripMenuItem.ForeColor = Color.White;

                outputToolStripMenuItem.ForeColor = Color.White;
                skinsToolStripMenuItem.ForeColor = Color.White;
                vehiclesToolStripMenuItem.ForeColor = Color.White;
                weaponsToolStripMenuItem.ForeColor = Color.White;

                compileToolStripMenuItem.ForeColor = Color.White;
                compileOptionsToolStripMenuItem.ForeColor = Color.White;

                aboutToolStripMenuItem.ForeColor = Color.White;
                #endregion

                #region TextEditor
                texteditor.TextBox.BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.ForeColor = Color.FromArgb(30, 30, 30);

                texteditor.TextBox.Styles[0].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[1].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[2].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[3].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[4].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[5].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[6].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[7].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[8].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[9].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[10].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[11].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[12].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[13].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[14].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[15].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[16].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[17].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[18].BackColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Styles[19].BackColor = Color.FromArgb(30, 30, 30);

                texteditor.TextBox.Styles[0].ForeColor = Color.White;
                texteditor.TextBox.Styles[1].ForeColor = Color.FromArgb(67, 166, 58);
                texteditor.TextBox.Styles[2].ForeColor = Color.FromArgb(67, 166, 58);
                texteditor.TextBox.Styles[3].ForeColor = Color.FromArgb(67, 166, 58);
                texteditor.TextBox.Styles[4].ForeColor = Color.FromArgb(181, 206, 168);
                texteditor.TextBox.Styles[5].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[6].ForeColor = Color.FromArgb(214, 157, 133);
                texteditor.TextBox.Styles[7].ForeColor = Color.FromArgb(214, 157, 133);
                texteditor.TextBox.Styles[8].ForeColor = Color.Blue;
                texteditor.TextBox.Styles[9].ForeColor = Color.FromArgb(155, 155, 155);
                texteditor.TextBox.Styles[10].ForeColor = Color.FromArgb(203, 221, 193);
                texteditor.TextBox.Styles[11].ForeColor = Color.White;
                texteditor.TextBox.Styles[12].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[13].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[14].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[15].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[16].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[17].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[18].ForeColor = Color.FromArgb(86, 156, 214);
                texteditor.TextBox.Styles[19].ForeColor = Color.FromArgb(86, 156, 214);

                texteditor.TextBox.Caret.Color = Color.White;

                texteditor.TextBox.Margins.FoldMarginColor = Color.FromArgb(30, 30, 30);
                texteditor.TextBox.Margins.FoldMarginHighlightColor = Color.FromArgb(30, 30, 30);
                #endregion

                #region Output
                output.OutBox.BackColor = Color.FromArgb(30, 30, 30);
                output.OutBox.ForeColor = Color.White;
                #endregion

                #region Skins
                skins.BackColor = Color.FromArgb(30, 30, 30);
                skins.ForeColor = Color.White;
                skins.SkinListView.BackColor = Color.FromArgb(30, 30, 30);
                skins.SkinListView.ForeColor = Color.White;
                #endregion

                #region Weapons
                weapons.BackColor = Color.FromArgb(30, 30, 30);
                weapons.ForeColor = Color.White;
                weapons.WeaponListView.BackColor = Color.FromArgb(30, 30, 30);
                weapons.WeaponListView.ForeColor = Color.White;
                #endregion

                #region Vehicles
                vehicles.BackColor = Color.FromArgb(30, 30, 30);
                vehicles.ForeColor = Color.White;
                vehicles.VehicleListView.BackColor = Color.FromArgb(30, 30, 30);
                vehicles.VehicleListView.ForeColor = Color.White;
                #endregion

            }

            if (ConfigFiles.ThemeChanged == true)
            {
                ConfigFiles.ThemeChanged = false;
                Application.Restart();
            }
            #endregion
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            LineLabel.Text = LineString + " " + (texteditor.TextBox.Lines.Current.Number + 1).ToString();
            ColumnLabel.Text = ColumnString + " " + (texteditor.TextBox.GetColumn(texteditor.TextBox.Caret.Position) + 1).ToString();

            if (ConfigFiles.ThemeChanged == true)
            {
                SetTheme();
            }

            if (ConfigFiles.LanguageChanged == true)
            {
                TranslateToLanguage();
            }

            #region CheckedBox
            if (output.IsHidden == false)
                outputToolStripMenuItem.Checked = true;
            else
                outputToolStripMenuItem.Checked = false;

            if (skins.IsHidden == false)
                skinsToolStripMenuItem.Checked = true;
            else
                skinsToolStripMenuItem.Checked = false;

            if (vehicles.IsHidden == false)
                vehiclesToolStripMenuItem.Checked = true;
            else
                vehiclesToolStripMenuItem.Checked = false;

            if (weapons.IsHidden == false)
                weaponsToolStripMenuItem.Checked = true;
            else
                weaponsToolStripMenuItem.Checked = false;
            #endregion
        }

        private void StatutsLabelUpdate_Tick(object sender, EventArgs e)
        {
            if (StatutsLabelCanBeChanged == true)
            {
                StatutsLabelCanBeChanged = false;
                StatutsLabel.Text = ReadyString;
                statusStrip1.BackColor = Color.FromArgb(0, 122, 204);
            }
        }

        public void TranslateToLanguage()
        {
            ConfigFiles.LanguageChanged = false;

            if (ConfigFiles.Language == "English")
            {
                SaveChangesTitleString = "Save changes";
                SaveChangesString = "You want to save your changes?";

                OpeningNewFileString = "Opening new file...";
                OpeningFileString = "Opening file...";
                ClosingFileString = "Closing file...";
                SavingFileString = "Saving file...";

                OpenedFileString = "File opened";
                SavedFileString = "File saved";
                ClosedFileString = "File closed";

                CompiledFinishText = "Compiled";
                ErrorsWhenCompiling = "Can't compile, here are some errors";

                NewVersionAvailableString = "A new version of PawnPlus is available";
                CurrentVersionString = "Current version is";

                CantCompile = "Can't compile an empty file.";

                ReadyString = "Ready";
                CompilingString = "Compiling...";
                LineString = "Line";
                ColumnString = "Column";

                if (CurrentVersion.Build > 0)
                BetaLabel.Text = "Version " + CurrentVersion.Major + "." + CurrentVersion.Minor + "." + CurrentVersion.Build + " beta";
                else
                    BetaLabel.Text = "Version " + CurrentVersion.Major + "." + CurrentVersion.Minor + " beta";

            }
            if (ConfigFiles.Language == "Romanian")
            {

                fileToolStripMenuItem.Text = "Fişier";
                newToolStripMenuItem.Text = "Nou";
                openToolStripMenuItem.Text = "Deschide fisier";
                saveToolStripMenuItem.Text = "Salvează";
                saveAsToolStripMenuItem.Text = "Salvează ca...";
                closeFileToolStripMenuItem.Text = "Închide fişier";
                exitToolStripMenuItem.Text = "Ieşi";

                editToolStripMenuItem.Text = "Editare";
                undoToolStripMenuItem.Text = "Înapoi";
                redoToolStripMenuItem.Text = "Înainte";
                cutToolStripMenuItem.Text = "Taie";
                copyToolStripMenuItem.Text = "Copiază";
                pasteToolStripMenuItem.Text = "Lipeşte";
                findToolStripMenuItem.Text = "Caută";
                findNextToolStripMenuItem.Text = "Caută următoarea";
                findPrevToolStripMenuItem.Text = "Caută precedenta";
                replaceToolStripMenuItem.Text = "Înlocuieşte";
                goToToolStripMenuItem.Text = "Du-te la";

                viewToolStripMenuItem.Text = "Vedere";
                outputToolStripMenuItem.Text = "Debug";
                skinsToolStripMenuItem.Text = "Skin-uri";
                vehiclesToolStripMenuItem.Text = "Vehicule";
                weaponsToolStripMenuItem.Text = "Arme";

                buildToolStripMenuItem.Text = "Compile";
                compileToolStripMenuItem.Text = "Compile";
                compileOptionsToolStripMenuItem.Text = "Opţiuni compilator";

                helpToolStripMenuItem.Text = "Ajutor";
                aboutToolStripMenuItem.Text = "Despre";

                texteditor.Text = "Code Editor";

                SaveChangesTitleString = "Salvează modificările";
                SaveChangesString = "Doreşti să salvezi modificările?";

                OpeningNewFileString = "Deschide fişier nou...";
                OpeningFileString = "Deschide fişier...";
                ClosingFileString = "Închide fişier...";
                SavingFileString = "Salvează fişier...";

                OpenedFileString = "Fişier deschis";
                SavedFileString = "Fişier salvat";
                ClosedFileString = "Fişier închis";

                CompiledFinishText = "Compilare terminată";
                ErrorsWhenCompiling = "Nu se poate compila, aici sunt unele erori";

                CantCompile = "Nu se poate compila un text gol.";

                ReadyString = "Ready";
                CompilingString = "Compilez...";
                LineString = "Linia";
                ColumnString = "Coloana";

                NewVersionAvailableString = "O noua versiune de PawnPlus este disponibilă";
                CurrentVersionString = "Versiunea curentă este";

                if (CurrentVersion.Build > 0)
                    BetaLabel.Text = "Versiunea " + CurrentVersion.Major + "." + CurrentVersion.Minor + "." + CurrentVersion.Build + " beta";
                else
                    BetaLabel.Text = "Versiunea " + CurrentVersion.Major + "." + CurrentVersion.Minor + " beta";
            }
            if (ConfigFiles.Language == "Spanish")
            {
 
                fileToolStripMenuItem.Text = "Archivo";
                newToolStripMenuItem.Text = "Nuevo";
                openToolStripMenuItem.Text = "Abrir";
                saveToolStripMenuItem.Text = "Guardar";
                saveAsToolStripMenuItem.Text = "Guardar como...";
                closeFileToolStripMenuItem.Text = "Cerrar archivo";
                exitToolStripMenuItem.Text = "Salir";

                editToolStripMenuItem.Text = "Editar";
                undoToolStripMenuItem.Text = "Deshacer";
                redoToolStripMenuItem.Text = "Rehacer";
                cutToolStripMenuItem.Text = "Cortar";
                copyToolStripMenuItem.Text = "Copiar";
                pasteToolStripMenuItem.Text = "Pegar";
                findToolStripMenuItem.Text = "Buscar";
                findNextToolStripMenuItem.Text = "Buscar siguiente";
                findPrevToolStripMenuItem.Text = "Buscar anterior";
                replaceToolStripMenuItem.Text = "Reemplazar";
                goToToolStripMenuItem.Text = "Ir a";

                viewToolStripMenuItem.Text = "Ver";
                outputToolStripMenuItem.Text = "Salida";
                skinsToolStripMenuItem.Text = "Skins";
                vehiclesToolStripMenuItem.Text = "Vehículos";
                weaponsToolStripMenuItem.Text = "Armas";

                buildToolStripMenuItem.Text = "Compilar";
                compileToolStripMenuItem.Text = "Compilar";
                compileOptionsToolStripMenuItem.Text = "Opciones de compilación";

                helpToolStripMenuItem.Text = "Ayuda";
                aboutToolStripMenuItem.Text = "Acerca de";

                texteditor.Text = "Editor de código";

                SaveChangesTitleString = "Guardar Cambios";
                SaveChangesString = "Usted quiere guardar los cambios?";

                OpeningNewFileString = "Abrir archivo nuevo...";
                OpeningFileString = "Abrir archivo...";
                ClosingFileString = "Archivo de cierre...";
                SavingFileString = "El archivo del ahorro...";

                OpenedFileString = "Archivo abierto";
                SavedFileString = "Archivo guardado";
                ClosedFileString = "Cerrado archivo";

                CompiledFinishText = "Compilado";
                ErrorsWhenCompiling = "No se puede compilar, aquí hay algunos errores";

                CantCompile = "No se puede compilar un archivo en blanco.";

                ReadyString = "Listo";
                CompilingString = "Compilando...";
                LineString = "Línea";
                ColumnString = "Columna";

                NewVersionAvailableString = "Hay una nueva versión de PawnPlus";
                CurrentVersionString = "Su versión actual es";

                if (CurrentVersion.Build > 0)
                    BetaLabel.Text = "Versión " + CurrentVersion.Major + "." + CurrentVersion.Minor + "." + CurrentVersion.Build + " beta";
                else
                    BetaLabel.Text = "Versión " + CurrentVersion.Major + "." + CurrentVersion.Minor + " beta";
            }
        }
    }
}
