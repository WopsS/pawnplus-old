﻿namespace PawnPlus
{
    partial class TextEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextEditor));
            this.TextBox = new ScintillaNET.Scintilla();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox.ConfigurationManager.Language = "cpp";
            this.TextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            this.TextBox.Lexing.LexerName = "cpp";
            this.TextBox.Lexing.LineCommentPrefix = "";
            this.TextBox.Lexing.StreamCommentPrefix = "";
            this.TextBox.Lexing.StreamCommentSufix = "";
            this.TextBox.Location = new System.Drawing.Point(0, 0);
            this.TextBox.Margins.Margin1.Width = 0;
            this.TextBox.Margins.Margin2.Width = 15;
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(784, 562);
            this.TextBox.Styles.BraceLight.BackColor = System.Drawing.Color.White;
            this.TextBox.Styles.ControlChar.BackColor = System.Drawing.Color.White;
            this.TextBox.Styles.IndentGuide.BackColor = System.Drawing.Color.White;
            this.TextBox.Styles.LastPredefined.BackColor = System.Drawing.Color.White;
            this.TextBox.TabIndex = 0;
            this.TextBox.CharAdded += new System.EventHandler<ScintillaNET.CharAddedEventArgs>(this.TextBox_CharAdded);
            this.TextBox.SelectionChanged += new System.EventHandler(this.TextBox_SelectionChanged);
            this.TextBox.TextDeleted += new System.EventHandler<ScintillaNET.TextModifiedEventArgs>(this.TextBox_TextDeleted);
            this.TextBox.TextInserted += new System.EventHandler<ScintillaNET.TextModifiedEventArgs>(this.TextBox_TextInserted);
            // 
            // TextEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.CloseButton = false;
            this.CloseButtonVisible = false;
            this.Controls.Add(this.TextBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TextEditor";
            this.Text = "Code Editor";
            this.Load += new System.EventHandler(this.TextEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public ScintillaNET.Scintilla TextBox;





    }
}