﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PawnPlus
{
    public class Include
    {
        public static string Functions = null;

        public static List<string> FirstAutoCompletItemsList = new List<string>();
        public static List<string> AutoCompletItems = new List<string>();

        public static string[] DefaultFiles = { "a_http.inc", "a_npc.inc", "a_objects.inc", "a_players.inc", "a_samp.inc", "a_sampdb.inc", "a_vehicles.inc", "core.inc", "datagram.inc", "file.inc", "float.inc", "string.inc", "time.inc", "sscanf2.inc", "streamer.inc" };

        public static string OpenedFile = null;
        public static string LastFileOpened = null;
        public static string PawnoFile = null;

        public static void ScriptingFunctions()
        {
            FirstAutoCompletItemsList.Clear();

            #region OldFunctions
            // Functions
            /*AutoCompletItems.Add("AddMenuItem");
            AutoCompletItems.Add("AddPlayerClass");
            AutoCompletItems.Add("AddPlayerClassEx");
            AutoCompletItems.Add("AddStaticPickup");
            AutoCompletItems.Add("AddStaticVehicle");
            AutoCompletItems.Add("AddStaticVehicleEx");
            AutoCompletItems.Add("AddVehicleComponent");
            AutoCompletItems.Add("AllowAdminTeleport");
            AutoCompletItems.Add("AllowInteriorWeapons");
            AutoCompletItems.Add("AllowPlayerTeleport");
            AutoCompletItems.Add("ApplyAnimation");
            AutoCompletItems.Add("Attach3DTextLabelToPlayer");
            AutoCompletItems.Add("Attach3DTextLabelToVehicle");
            AutoCompletItems.Add("AttachCameraToObject");
            AutoCompletItems.Add("AttachCameraToPlayerObject");
            AutoCompletItems.Add("AttachObjectToObject");
            AutoCompletItems.Add("AttachObjectToPlayer");
            AutoCompletItems.Add("AttachObjectToVehicle");
            AutoCompletItems.Add("AttachPlayerObjectToPlayer");
            AutoCompletItems.Add("AttachPlayerObjectToVehicle");
            AutoCompletItems.Add("AttachTrailerToVehicle");
            AutoCompletItems.Add("Ban");
            AutoCompletItems.Add("BanEx");
            AutoCompletItems.Add("CallLocalFunction");
            AutoCompletItems.Add("CallRemoteFunction");
            AutoCompletItems.Add("CancelEdit");
            AutoCompletItems.Add("CancelSelectTextDraw");
            AutoCompletItems.Add("ChangeVehicleColor");
            AutoCompletItems.Add("ChangeVehiclePaintjob");
            AutoCompletItems.Add("Clamp");
            AutoCompletItems.Add("ClearAnimations");
            AutoCompletItems.Add("ConnectNPC");
            AutoCompletItems.Add("Create3DTextLabel");
            AutoCompletItems.Add("CreateExplosion");
            AutoCompletItems.Add("CreateMenu");
            AutoCompletItems.Add("CreateObject");
            AutoCompletItems.Add("CreatePickup");
            AutoCompletItems.Add("CreatePlayer3DTextLabel");
            AutoCompletItems.Add("CreatePlayerObject");
            AutoCompletItems.Add("CreatePlayerTextDraw");
            AutoCompletItems.Add("CreateVehicle");
            AutoCompletItems.Add("db_close");
            AutoCompletItems.Add("db_num_fields");
            AutoCompletItems.Add("db_free_result");
            AutoCompletItems.Add("db_get_field");
            AutoCompletItems.Add("db_get_field_assoc");
            AutoCompletItems.Add("db_next_row");
            AutoCompletItems.Add("db_num_fields");
            AutoCompletItems.Add("db_num_rows");
            AutoCompletItems.Add("db_open");
            AutoCompletItems.Add("db_query");
            AutoCompletItems.Add("Delete3DTextLabel");
            AutoCompletItems.Add("DeletePVar");
            AutoCompletItems.Add("DeletePlayer3DTextLabel");
            AutoCompletItems.Add("Deleteproperty");
            AutoCompletItems.Add("DestroyMenu");
            AutoCompletItems.Add("DestroyObject");
            AutoCompletItems.Add("DestroyPickup");
            AutoCompletItems.Add("DestroyPlayerObject");
            AutoCompletItems.Add("DestroyVehicle");
            AutoCompletItems.Add("DetachTrailerFromVehicle");
            AutoCompletItems.Add("DisableInteriorEnterExits");
            AutoCompletItems.Add("DisableMenu");
            AutoCompletItems.Add("DisableMenuRow");
            AutoCompletItems.Add("DisableNameTagLOS");
            AutoCompletItems.Add("DisablePlayerCheckpoint");
            AutoCompletItems.Add("DisablePlayerRaceCheckpoint");
            AutoCompletItems.Add("EditAttachedObject");
            AutoCompletItems.Add("EditObject");
            AutoCompletItems.Add("EditPlayerObject");
            AutoCompletItems.Add("EnableStuntBonusForAll");
            AutoCompletItems.Add("EnableStuntBonusForPlayer");
            AutoCompletItems.Add("EnableTirePopping");
            AutoCompletItems.Add("EnableVehicleFriendlyFire");
            AutoCompletItems.Add("EnableZoneNames");
            AutoCompletItems.Add("Existproperty");
            AutoCompletItems.Add("Fblockread");
            AutoCompletItems.Add("Fblockwrite");
            AutoCompletItems.Add("fclose");
            AutoCompletItems.Add("fexist");
            AutoCompletItems.Add("fgetchar");
            AutoCompletItems.Add("flength");
            AutoCompletItems.Add("floatabs");
            AutoCompletItems.Add("floatadd");
            AutoCompletItems.Add("floatattan");
            AutoCompletItems.Add("floatcmp");
            AutoCompletItems.Add("floatcos");
            AutoCompletItems.Add("floatdiv");
            AutoCompletItems.Add("floatfract");
            AutoCompletItems.Add("floatlog");
            AutoCompletItems.Add("floatmul");
            AutoCompletItems.Add("floatpower");
            AutoCompletItems.Add("floatround");
            AutoCompletItems.Add("floatsin");
            AutoCompletItems.Add("floatsqroot");
            AutoCompletItems.Add("floatstr");
            AutoCompletItems.Add("floatsub");
            AutoCompletItems.Add("floattan");
            AutoCompletItems.Add("Fmatch");
            AutoCompletItems.Add("fopen");
            AutoCompletItems.Add("forceClassSelection");
            AutoCompletItems.Add("format");
            AutoCompletItems.Add("Fputchar");
            AutoCompletItems.Add("fread");
            AutoCompletItems.Add("fremove");
            AutoCompletItems.Add("fseek");
            AutoCompletItems.Add("ftemp");
            AutoCompletItems.Add("funcidx");
            AutoCompletItems.Add("fwrite");
            AutoCompletItems.Add("GameModeExit");
            AutoCompletItems.Add("GameTextForAll");
            AutoCompletItems.Add("GameTextForPlayer");
            AutoCompletItems.Add("GangZoneCreate");
            AutoCompletItems.Add("GangZoneDestroy");
            AutoCompletItems.Add("GangZoneFlashForAll");
            AutoCompletItems.Add("GangZoneFlashForPlayer");
            AutoCompletItems.Add("GangZoneHideForAll");
            AutoCompletItems.Add("GangZoneHideForPlayer");
            AutoCompletItems.Add("GangZoneShowForAll");
            AutoCompletItems.Add("GangZoneShowForPlayer");
            AutoCompletItems.Add("GangZoneStopFlashForAll");
            AutoCompletItems.Add("GangZoneStopFlashForPlayer");
            AutoCompletItems.Add("GetAnimationName");
            AutoCompletItems.Add("GetGravity");
            AutoCompletItems.Add("GetMaxPlayers");
            AutoCompletItems.Add("GetNetworkStats");
            AutoCompletItems.Add("GetObjectPos");
            AutoCompletItems.Add("GetObjectRot");
            AutoCompletItems.Add("GetPVarFloat");
            AutoCompletItems.Add("GetPVarInt");
            AutoCompletItems.Add("GetPVarNameAtIndex");
            AutoCompletItems.Add("GetPVarString");
            AutoCompletItems.Add("GetPVarType");
            AutoCompletItems.Add("GetPVarsUpperIndex");
            AutoCompletItems.Add("GetPlayerAmmo");
            AutoCompletItems.Add("GetPlayerAnimationIndex");
            AutoCompletItems.Add("GetPlayerArmour");
            AutoCompletItems.Add("GetPlayerCameraFrontVector");
            AutoCompletItems.Add("GetPlayerCameraMode");
            AutoCompletItems.Add("GetPlayerCameraPos");
            AutoCompletItems.Add("GetPlayerCameraUpVector");
            AutoCompletItems.Add("GetPlayerColor");
            AutoCompletItems.Add("GetPlayerDistanceFromPoint");
            AutoCompletItems.Add("GetPlayerDrunkLevel");
            AutoCompletItems.Add("GetPlayerFacingAngle");
            AutoCompletItems.Add("GetPlayerFightingStyle");
            AutoCompletItems.Add("GetPlayerHealth");
            AutoCompletItems.Add("GetPlayerInterior");
            AutoCompletItems.Add("GetPlayerIp");
            AutoCompletItems.Add("GetPlayerKeys");
            AutoCompletItems.Add("GetPlayerMenu");
            AutoCompletItems.Add("GetPlayerMoney");
            AutoCompletItems.Add("GetPlayerName");
            AutoCompletItems.Add("GetPlayerNetworkStats");
            AutoCompletItems.Add("GetPlayerObjectPos");
            AutoCompletItems.Add("GetPlayerObjectRot");
            AutoCompletItems.Add("GetPlayerPing");
            AutoCompletItems.Add("GetPlayerPos");
            AutoCompletItems.Add("GetPlayerScore");
            AutoCompletItems.Add("GetPlayerSkin");
            AutoCompletItems.Add("GetPlayerSpecialAction");
            AutoCompletItems.Add("GetPlayerSpecialAction FR");
            AutoCompletItems.Add("GetPlayerState");
            AutoCompletItems.Add("GetPlayerSurfingObjectID");
            AutoCompletItems.Add("GetPlayerSurfingVehicleID");
            AutoCompletItems.Add("GetPlayerTargetPlayer");
            AutoCompletItems.Add("GetPlayerTeam");
            AutoCompletItems.Add("GetPlayerTime");
            AutoCompletItems.Add("GetPlayerVehicleID");
            AutoCompletItems.Add("GetPlayerVehicleSeat");
            AutoCompletItems.Add("GetPlayerVelocity");
            AutoCompletItems.Add("GetPlayerVersion");
            AutoCompletItems.Add("GetPlayerVirtualWorld");
            AutoCompletItems.Add("GetPlayerWantedLevel");
            AutoCompletItems.Add("GetPlayerWeapon");
            AutoCompletItems.Add("GetPlayerWeaponData");
            AutoCompletItems.Add("GetPlayerWeaponState");
            AutoCompletItems.Add("GetServerVarAsBool");
            AutoCompletItems.Add("GetServerVarAsInt");
            AutoCompletItems.Add("GetServerVarAsString");
            AutoCompletItems.Add("GetTickCount");
            AutoCompletItems.Add("GetVehicleComponentInSlot");
            AutoCompletItems.Add("GetVehicleComponentType");
            AutoCompletItems.Add("GetVehicleDamageStatus");
            AutoCompletItems.Add("GetVehicleDistanceFromPoint");
            AutoCompletItems.Add("GetVehicleHealth");
            AutoCompletItems.Add("GetVehicleModel");
            AutoCompletItems.Add("GetVehicleModelInfo");
            AutoCompletItems.Add("GetVehiclePos");
            AutoCompletItems.Add("GetVehicleRotationQuat");
            AutoCompletItems.Add("GetVehicleTrailer");
            AutoCompletItems.Add("GetVehicleVelocity");
            AutoCompletItems.Add("GetVehicleVirtualWorld");
            AutoCompletItems.Add("GetVehicleZAngle");
            AutoCompletItems.Add("GetWeaponName");
            AutoCompletItems.Add("Getarg");
            AutoCompletItems.Add("Getdate");
            AutoCompletItems.Add("Getproperty");
            AutoCompletItems.Add("Gettime");
            AutoCompletItems.Add("GivePlayerWeapon");
            AutoCompletItems.Add("HTTP");
            AutoCompletItems.Add("Heapspace");
            AutoCompletItems.Add("HideMenuForPlayer");
            AutoCompletItems.Add("InterpolateCameraLookAt");
            AutoCompletItems.Add("InterpolateCameraPos");
            AutoCompletItems.Add("IsObjectMoving");
            AutoCompletItems.Add("IsPlayerAdmin");
            AutoCompletItems.Add("IsPlayerAttachedObjectSlotUsed");
            AutoCompletItems.Add("IsPlayerConnected");
            AutoCompletItems.Add("IsPlayerHoldingObject");
            AutoCompletItems.Add("IsPlayerInAnyVehicle");
            AutoCompletItems.Add("IsPlayerInCheckpoint");
            AutoCompletItems.Add("IsPlayerInRaceCheckpoint");
            AutoCompletItems.Add("IsPlayerInRangeOfPoint");
            AutoCompletItems.Add("IsPlayerInVehicle");
            AutoCompletItems.Add("IsPlayerNPC");
            AutoCompletItems.Add("IsPlayerObjectMoving");
            AutoCompletItems.Add("IsPlayerStreamedIn");
            AutoCompletItems.Add("IsTrailerAttachedToVehicle");
            AutoCompletItems.Add("IsValidObject");
            AutoCompletItems.Add("IsValidPlayerObject");
            AutoCompletItems.Add("IsValidVehicle");
            AutoCompletItems.Add("IsVehicleStreamedIn");
            AutoCompletItems.Add("Ispacked");
            AutoCompletItems.Add("Kick");
            AutoCompletItems.Add("KillTimer");
            AutoCompletItems.Add("LimitGlobalChatRadius");
            AutoCompletItems.Add("LimitPlayerMarkerRadius");
            AutoCompletItems.Add("LinkVehicleToInterior");
            AutoCompletItems.Add("ManualVehicleEngineAndLights");
            AutoCompletItems.Add("Memcpy");
            AutoCompletItems.Add("MoveObject");
            AutoCompletItems.Add("MovePlayerObject");
            AutoCompletItems.Add("OnNPCConnect");
            AutoCompletItems.Add("OnNPCDisconnect");
            AutoCompletItems.Add("OnNPCEnterVehicle");
            AutoCompletItems.Add("OnNPCExitVehicle");
            AutoCompletItems.Add("OnNPCModeExit");
            AutoCompletItems.Add("OnNPCModeInit");
            AutoCompletItems.Add("OnNPCSpawn");
            AutoCompletItems.Add("PauseRecordingPlayback");
            AutoCompletItems.Add("ResumeRecordingPlayback");
            AutoCompletItems.Add("SendChat");
            AutoCompletItems.Add("SendCommand");
            AutoCompletItems.Add("StartRecordingPlayback");
            AutoCompletItems.Add("StopRecordingPlayback");
            AutoCompletItems.Add("PlayAudioStreamForPlayer");
            AutoCompletItems.Add("PlayAudioStreamForPlayer FR");
            AutoCompletItems.Add("PlayCrimeReportForPlayer");
            AutoCompletItems.Add("PlayerPlaySound");
            AutoCompletItems.Add("PlayerSpectatePlayer");
            AutoCompletItems.Add("PlayerSpectateVehicle");
            AutoCompletItems.Add("PlayerTextDrawAlignment");
            AutoCompletItems.Add("PlayerTextDrawBackgroundColor");
            AutoCompletItems.Add("PlayerTextDrawBoxColor");
            AutoCompletItems.Add("PlayerTextDrawColor");
            AutoCompletItems.Add("PlayerTextDrawDestroy");
            AutoCompletItems.Add("PlayerTextDrawFont");
            AutoCompletItems.Add("PlayerTextDrawHide");
            AutoCompletItems.Add("PlayerTextDrawLetterSize");
            AutoCompletItems.Add("PlayerTextDrawSetOutline");
            AutoCompletItems.Add("PlayerTextDrawSetPreviewModel");
            AutoCompletItems.Add("PlayerTextDrawSetPreviewRot");
            AutoCompletItems.Add("PlayerTextDrawSetPreviewVehCol");
            AutoCompletItems.Add("PlayerTextDrawSetProportional");
            AutoCompletItems.Add("PlayerTextDrawSetSelectable");
            AutoCompletItems.Add("PlayerTextDrawSetShadow");
            AutoCompletItems.Add("PlayerTextDrawSetString");
            AutoCompletItems.Add("PlayerTextDrawShow");
            AutoCompletItems.Add("PlayerTextDrawTextSize");
            AutoCompletItems.Add("PlayerTextDrawUseBox");
            AutoCompletItems.Add("print");
            AutoCompletItems.Add("printf");
            AutoCompletItems.Add("PutPlayerInVehicle");
            AutoCompletItems.Add("random");
            AutoCompletItems.Add("RemoveBuildingForPlayer");
            AutoCompletItems.Add("RemovePlayerAttachedObject");
            AutoCompletItems.Add("RemovePlayerFromVehicle");
            AutoCompletItems.Add("RemovePlayerMapIcon");
            AutoCompletItems.Add("RemoveVehicleComponent");
            AutoCompletItems.Add("RepairVehicle");
            AutoCompletItems.Add("ResetPlayerMoney");
            AutoCompletItems.Add("ResetPlayerWeapons");
            AutoCompletItems.Add("SelectObject");
            AutoCompletItems.Add("SelectTextDraw");
            AutoCompletItems.Add("SendClientMessage");
            AutoCompletItems.Add("SendClientMessageToAll");
            AutoCompletItems.Add("SendDeathMessage");
            AutoCompletItems.Add("SendPlayerMessageToAll");
            AutoCompletItems.Add("SendPlayerMessageToPlayer");
            AutoCompletItems.Add("SendRconCommand");
            AutoCompletItems.Add("SetCameraBehindPlayer");
            AutoCompletItems.Add("SetDeathDropAmount");
            AutoCompletItems.Add("SetDisabledWeapons");
            AutoCompletItems.Add("SetGameModeText");
            AutoCompletItems.Add("SetGravity");
            AutoCompletItems.Add("SetMenuColumnHeader");
            AutoCompletItems.Add("SetNameTagDrawDistance");
            AutoCompletItems.Add("SetObjectMaterial");
            AutoCompletItems.Add("SetObjectMaterialText");
            AutoCompletItems.Add("SetObjectPos");
            AutoCompletItems.Add("SetObjectRot");
            AutoCompletItems.Add("SetPVarFloat");
            AutoCompletItems.Add("SetPVarInt");
            AutoCompletItems.Add("SetPVarString");
            AutoCompletItems.Add("SetPlayerAmmo");
            AutoCompletItems.Add("SetPlayerArmedWeapon");
            AutoCompletItems.Add("SetPlayerArmour");
            AutoCompletItems.Add("SetPlayerAttachedObject");
            AutoCompletItems.Add("SetPlayerCameraLookAt");
            AutoCompletItems.Add("SetPlayerCameraPos");
            AutoCompletItems.Add("SetPlayerChatBubble");
            AutoCompletItems.Add("SetPlayerCheckpoint");
            AutoCompletItems.Add("SetPlayerColor");
            AutoCompletItems.Add("SetPlayerDrunkLevel");
            AutoCompletItems.Add("SetPlayerFacingAngle");
            AutoCompletItems.Add("SetPlayerFightingStyle");
            AutoCompletItems.Add("SetPlayerHealth");
            AutoCompletItems.Add("SetPlayerHoldingObject");
            AutoCompletItems.Add("SetPlayerInterior");
            AutoCompletItems.Add("SetPlayerMapIcon");
            AutoCompletItems.Add("SetPlayerMarkerForPlayer");
            AutoCompletItems.Add("SetPlayerName");
            AutoCompletItems.Add("SetPlayerObjectMaterial");
            AutoCompletItems.Add("SetPlayerObjectMaterialText");
            AutoCompletItems.Add("SetPlayerObjectPos");
            AutoCompletItems.Add("SetPlayerObjectRot");
            AutoCompletItems.Add("SetPlayerPos");
            AutoCompletItems.Add("SetPlayerPosFindZ");
            AutoCompletItems.Add("SetPlayerRaceCheckpoint");
            AutoCompletItems.Add("SetPlayerScore");
            AutoCompletItems.Add("SetPlayerShopName");
            AutoCompletItems.Add("SetPlayerSkillLevel");
            AutoCompletItems.Add("SetPlayerSkin");
            AutoCompletItems.Add("SetPlayerSpecialAction");
            AutoCompletItems.Add("SetPlayerTeam");
            AutoCompletItems.Add("SetPlayerTime");
            AutoCompletItems.Add("SetPlayerVelocity");
            AutoCompletItems.Add("SetPlayerVirtualWorld");
            AutoCompletItems.Add("SetPlayerWantedLevel");
            AutoCompletItems.Add("SetPlayerWeather");
            AutoCompletItems.Add("SetPlayerWorldBounds");
            AutoCompletItems.Add("SetSpawnInfo");
            AutoCompletItems.Add("SetTeamCount");
            AutoCompletItems.Add("SetTimer");
            AutoCompletItems.Add("SetTimerEx");
            AutoCompletItems.Add("SetVehicleAngularVelocity");
            AutoCompletItems.Add("SetVehicleHealth");
            AutoCompletItems.Add("SetVehicleNumberPlate");
            AutoCompletItems.Add("SetVehicleParamsEx");
            AutoCompletItems.Add("SetVehicleParamsForPlayer");
            AutoCompletItems.Add("SetVehiclePos");
            AutoCompletItems.Add("SetVehicleToRespawn");
            AutoCompletItems.Add("SetVehicleVelocity");
            AutoCompletItems.Add("SetVehicleVirtualWorld");
            AutoCompletItems.Add("SetVehicleZAngle");
            AutoCompletItems.Add("SetWeather");
            AutoCompletItems.Add("SetWorldTime");
            AutoCompletItems.Add("Setproperty");
            AutoCompletItems.Add("ShowMenuForPlayer");
            AutoCompletItems.Add("ShowNameTags");
            AutoCompletItems.Add("ShowPlayerDialog");
            AutoCompletItems.Add("ShowPlayerMarkers");
            AutoCompletItems.Add("ShowPlayerNameTagForPlayer");
            AutoCompletItems.Add("SpawnPlayer");
            AutoCompletItems.Add("StartRecordingPlayerData");
            AutoCompletItems.Add("StopAudioStreamForPlayer");
            AutoCompletItems.Add("StopObject");
            AutoCompletItems.Add("StopPlayerHoldingObject");
            AutoCompletItems.Add("StopPlayerObject");
            AutoCompletItems.Add("StopRecordingPlayerData");
            AutoCompletItems.Add("strcat");
            AutoCompletItems.Add("strcmp");
            AutoCompletItems.Add("strdel");
            AutoCompletItems.Add("strfind");
            AutoCompletItems.Add("strins");
            AutoCompletItems.Add("strlen");
            AutoCompletItems.Add("strmid");
            AutoCompletItems.Add("strpack");
            AutoCompletItems.Add("strunpack");
            AutoCompletItems.Add("strval");
            AutoCompletItems.Add("TextDrawAlignment");
            AutoCompletItems.Add("TextDrawBackgroundColor");
            AutoCompletItems.Add("TextDrawBoxColor");
            AutoCompletItems.Add("TextDrawColor");
            AutoCompletItems.Add("TextDrawCreate");
            AutoCompletItems.Add("TextDrawDestroy");
            AutoCompletItems.Add("TextDrawFont");
            AutoCompletItems.Add("TextDrawHideForAll");
            AutoCompletItems.Add("TextDrawHideForPlayer");
            AutoCompletItems.Add("TextDrawLetterSize");
            AutoCompletItems.Add("TextDrawSetOutline");
            AutoCompletItems.Add("TextDrawSetPreviewModel");
            AutoCompletItems.Add("TextDrawSetPreviewRot");
            AutoCompletItems.Add("TextDrawSetPreviewVehCol");
            AutoCompletItems.Add("TextDrawSetProportional");
            AutoCompletItems.Add("TextDrawSetSelectable");
            AutoCompletItems.Add("TextDrawSetShadow");
            AutoCompletItems.Add("TextDrawSetString");
            AutoCompletItems.Add("TextDrawShowForAll");
            AutoCompletItems.Add("TextDrawShowForPlayer");
            AutoCompletItems.Add("TextDrawTextSize");
            AutoCompletItems.Add("TextDrawUseBox");
            AutoCompletItems.Add("Tickcount");
            AutoCompletItems.Add("TogglePlayerClock");
            AutoCompletItems.Add("TogglePlayerControllable");
            AutoCompletItems.Add("TogglePlayerSpectating");
            AutoCompletItems.Add("Update3DTextLabelText");
            AutoCompletItems.Add("UpdatePlayer3DTextLabelText");
            AutoCompletItems.Add("UpdateVehicleDamageStatus");
            AutoCompletItems.Add("UsePlayerPedAnims");
            AutoCompletItems.Add("Uudecode");
            AutoCompletItems.Add("Uuencode");
            AutoCompletItems.Add("Valstr");
            AutoCompletItems.Add("VectorSize");

            // Callbacks
            AutoCompletItems.Add("OnClientMessage");
            AutoCompletItems.Add("OnPlayerDeath");
            AutoCompletItems.Add("OnPlayerStreamIn");
            AutoCompletItems.Add("OnPlayerStreamOut");
            AutoCompletItems.Add("OnPlayerText");
            AutoCompletItems.Add("OnRecordingPlaybackEnd");
            AutoCompletItems.Add("OnVehicleStreamIn");
            AutoCompletItems.Add("OnVehicleStreamOut");
            AutoCompletItems.Add("OnDialogResponse");
            AutoCompletItems.Add("OnEnterExitModShop");
            AutoCompletItems.Add("OnFilterScriptExit");
            AutoCompletItems.Add("OnFilterScriptInit");
            AutoCompletItems.Add("OnGameModeExit");
            AutoCompletItems.Add("OnGameModeInit");
            AutoCompletItems.Add("OnObjectMoved");
            AutoCompletItems.Add("OnPlayerClickMap");
            AutoCompletItems.Add("OnPlayerClickPlayer");
            AutoCompletItems.Add("OnPlayerClickPlayerTextDraw");
            AutoCompletItems.Add("OnPlayerClickTextDraw");
            AutoCompletItems.Add("OnPlayerCommandText");
            AutoCompletItems.Add("OnPlayerConnect");
            AutoCompletItems.Add("OnPlayerDeath");
            AutoCompletItems.Add("OnPlayerDisconnect");
            AutoCompletItems.Add("OnPlayerEditAttachedObject");
            AutoCompletItems.Add("OnPlayerEditObject");
            AutoCompletItems.Add("OnPlayerEnterCheckpoint");
            AutoCompletItems.Add("OnPlayerEnterRaceCheckpoint");
            AutoCompletItems.Add("OnPlayerEnterVehicle");
            AutoCompletItems.Add("OnPlayerExitVehicle");
            AutoCompletItems.Add("OnPlayerExitedMenu");
            AutoCompletItems.Add("OnPlayerGiveDamage");
            AutoCompletItems.Add("OnPlayerInteriorChange");
            AutoCompletItems.Add("OnPlayerKeyStateChange");
            AutoCompletItems.Add("OnPlayerLeaveCheckpoint");
            AutoCompletItems.Add("OnPlayerLeaveRaceCheckpoint");
            AutoCompletItems.Add("OnPlayerObjectMoved");
            AutoCompletItems.Add("OnPlayerPickUpPickup");
            AutoCompletItems.Add("OnPlayerPrivmsg");
            AutoCompletItems.Add("OnPlayerRequestClass");
            AutoCompletItems.Add("OnPlayerRequestSpawn");
            AutoCompletItems.Add("OnPlayerSelectObject");
            AutoCompletItems.Add("OnPlayerSelectedMenuRow");
            AutoCompletItems.Add("OnPlayerSpawn");
            AutoCompletItems.Add("OnPlayerStateChange");
            AutoCompletItems.Add("OnPlayerStreamIn");
            AutoCompletItems.Add("OnPlayerStreamOut");
            AutoCompletItems.Add("OnPlayerTakeDamage");
            AutoCompletItems.Add("OnPlayerTeamPrivmsg");
            AutoCompletItems.Add("OnPlayerText");
            AutoCompletItems.Add("OnPlayerUpdate");
            AutoCompletItems.Add("OnRconCommand");
            AutoCompletItems.Add("OnRconLoginAttempt");
            AutoCompletItems.Add("OnUnoccupiedVehicleUpdate");
            AutoCompletItems.Add("OnVehicleDamageStatusUpdate");
            AutoCompletItems.Add("OnVehicleDeath");
            AutoCompletItems.Add("OnVehicleMod");
            AutoCompletItems.Add("OnVehiclePaintjob");
            AutoCompletItems.Add("OnVehicleRespray");
            AutoCompletItems.Add("OnVehicleSpawn");
            AutoCompletItems.Add("OnVehicleStreamIn");
            AutoCompletItems.Add("OnVehicleStreamOut");

            // Default functions
            AutoCompletItems.Add("public");
            AutoCompletItems.Add("new");
            AutoCompletItems.Add("int" + "?test");
            AutoCompletItems.Add("float");
            AutoCompletItems.Add("double");
            AutoCompletItems.Add("forward");
            AutoCompletItems.Add("define");
            AutoCompletItems.Add("include");
            AutoCompletItems.Add("if");
            AutoCompletItems.Add("else");
            AutoCompletItems.Add("for");
            AutoCompletItems.Add("while");
            AutoCompletItems.Add("else");
            AutoCompletItems.Add("stock");
            AutoCompletItems.Add("bool");*/
            #endregion

            #region DefaultFiles
            for (int i = 0; i < 15; i++)
            {
                if (File.Exists(PawnoFile + DefaultFiles[i]))
                {
                    Functions = File.ReadAllText(PawnoFile + DefaultFiles[i]);
                    if (Functions.Contains("native") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(native)\s(.+?)\((.+)\)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                }
            }
            #endregion

            #region CustomFiles
            foreach (string CustomFiles in Directory.EnumerateFiles(PawnoFile, "*.inc"))
            {
                if (Path.GetFileName(CustomFiles) != DefaultFiles[0] && Path.GetFileName(CustomFiles) != DefaultFiles[1] && Path.GetFileName(CustomFiles) != DefaultFiles[2] && Path.GetFileName(CustomFiles) != DefaultFiles[3] && Path.GetFileName(CustomFiles) != DefaultFiles[4] && Path.GetFileName(CustomFiles) != DefaultFiles[5] && Path.GetFileName(CustomFiles) != DefaultFiles[6] && Path.GetFileName(CustomFiles) != DefaultFiles[7] && Path.GetFileName(CustomFiles) != DefaultFiles[8] && Path.GetFileName(CustomFiles) != DefaultFiles[9] && Path.GetFileName(CustomFiles) != DefaultFiles[10] && Path.GetFileName(CustomFiles) != DefaultFiles[11] && Path.GetFileName(CustomFiles) != DefaultFiles[12] && Path.GetFileName(CustomFiles) != DefaultFiles[13] && Path.GetFileName(CustomFiles) != DefaultFiles[14])
                {
                    Functions = File.ReadAllText(CustomFiles);

                    #region native
                    if (Functions.Contains("native") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(native)\s(.+?)\((.+)\)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region define
                    if (Functions.Contains("define") == true && Path.GetFileName(CustomFiles) != "a_mysql.inc")
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(define)\s(.+)\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\s+\d+", "");
                            outputs = Regex.Replace(outputs, @"\(\d+\)", "");
                            outputs = Regex.Replace(outputs, @"\s+", "");
                            outputs = Regex.Replace(outputs, @"//", "");
                            outputs = Regex.Replace(outputs, @"\((.+)\)", "");
                            outputs = Regex.Replace(outputs, "\"([^\"]*)\"", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region forward
                    if (Functions.Contains("forward") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(forward)\s(.+?)\((.+)\)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region public
                    if (Functions.Contains("public") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(public)\s(.+?)\((.+)\)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region stock
                    if (Functions.Contains("stock") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(public)\s(.+?)\((.+)\)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region enum
                    if (Functions.Contains("enum") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"enum\s+\w+\s*(?<enumBody>\{[\S|\s|]+?\})"))
                        {
                            foreach (Match SecondMatch in Regex.Matches(FirstMatch.Groups[1].ToString(), @"\s+(.+)"))
                            {
                                string outputs = Regex.Replace(SecondMatch.Groups[1].ToString(), @"\[(.+)\]", "");
                                outputs = Regex.Replace(outputs, @"\,\s+", "\r\n");
                                outputs = Regex.Replace(outputs, @"\}", "");

                                foreach (Match FinalMatch in Regex.Matches(outputs, @"(.+)\r\n"))
                                {
                                    FirstAutoCompletItemsList.Add(FinalMatch.Groups[1].ToString());
                                }

                            }
                        }
                    }
                    #endregion

                    #region new
                    if (Functions.Contains("new") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(new)\s(.+?)\;\r\n"))
                        {
                            string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\[(.+?)\]", "");
                            FirstAutoCompletItemsList.Add(outputs);
                        }
                    }
                    #endregion

                    #region int
                    if (Functions.Contains("int") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(int)\s(.+?)\;\r\n"))
                        {
                            FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                        }
                    }
                    #endregion

                    #region float
                    if (Functions.Contains("float") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(float)\s(.+?)\;\r\n"))
                        {
                            FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                        }
                    }
                    #endregion

                    #region bool
                    if (Functions.Contains("bool") == true)
                    {
                        foreach (Match FirstMatch in Regex.Matches(Functions, @"(bool)\s(.+?)\;\r\n"))
                        {
                            FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                        }
                    }
                    #endregion
                }
            }
            #endregion

            #region GamemodeInclude
            Functions = File.ReadAllText(OpenedFile);

            #region native
            if (Functions.Contains("native") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(native)\s(.+?)\((.+)\)\;\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region define
            if (Functions.Contains("define") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(define)\s(.+)\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\s+\d+", "");
                    outputs = Regex.Replace(outputs, @"\(\d+\)", "");
                    outputs = Regex.Replace(outputs, @"\s+", "");
                    outputs = Regex.Replace(outputs, @"//", "");
                    outputs = Regex.Replace(outputs, @"\((.+)\)", "");
                    outputs = Regex.Replace(outputs, "\"([^\"]*)\"", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region forward
            if (Functions.Contains("forward") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(forward)\s(.+?)\((.+)\)\;\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region public
            if (Functions.Contains("public") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(public)\s(.+?)\((.+)\)\;\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region stock
            if (Functions.Contains("stock") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(public)\s(.+?)\((.+)\)\;\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"(.+?):", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region enum
            if (Functions.Contains("enum") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"enum\s+\w+\s*(?<enumBody>\{[\S|\s|]+?\})"))
                {
                    foreach (Match SecondMatch in Regex.Matches(FirstMatch.Groups[1].ToString(), @"\s+(.+)"))
                    {
                        string outputs = Regex.Replace(SecondMatch.Groups[1].ToString(), @"\[(.+)\]", "");
                        outputs = Regex.Replace(outputs, @"\,\s+", "\r\n");
                        outputs = Regex.Replace(outputs, @"\}", "");

                        foreach (Match FinalMatch in Regex.Matches(outputs, @"(.+)\r\n"))
                        {
                            FirstAutoCompletItemsList.Add(FinalMatch.Groups[1].ToString());
                        }

                    }
                }
            }
            #endregion

            #region new
            if (Functions.Contains("new") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(new)\s(.+)\s+\=(.+)\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\[(.+?)\]", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(new)\s(.+)\;(.+)\,\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\[(.+?)\]", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(new)\s(.+)\;(.+)\r\n"))
                {
                    string outputs = Regex.Replace(FirstMatch.Groups[2].ToString(), @"\[(.+?)\]", "");
                    FirstAutoCompletItemsList.Add(outputs);
                }
            }
            #endregion

            #region int
            if (Functions.Contains("int") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(int)\s(.+?)\;\r\n"))
                {
                    FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                }
            }
            #endregion

            #region float
            if (Functions.Contains("float") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(float)\s(.+?)\;\r\n"))
                {
                    FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                }
            }
            #endregion

            #region bool
            if (Functions.Contains("bool") == true)
            {
                foreach (Match FirstMatch in Regex.Matches(Functions, @"(bool)\s(.+?)\;\r\n"))
                {
                    FirstAutoCompletItemsList.Add(FirstMatch.Groups[2].ToString());
                }
            }
            #endregion
            #endregion

            var DistinctItems = FirstAutoCompletItemsList.GroupBy(x => x).Select(y => y.First());

            foreach (var Duplicate in DistinctItems)
                AutoCompletItems.Add(Duplicate);
        }
    }
}