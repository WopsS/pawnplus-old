﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using WeifenLuo.WinFormsUI.Docking;
using System.Xml;

namespace PawnPlus
{
    class GreyTheme : ProfessionalColorTable
    {
        #region Menu

        public override Color MenuItemSelected
        {
            get { return Color.FromArgb(51, 51, 52); }
        }

        public override Color MenuBorder
        {
            get { return Color.FromArgb(51, 51, 52); }
        }

        public override Color MenuItemSelectedGradientBegin
        { get { return Color.FromArgb(51, 51, 52); } }

        public override Color MenuItemSelectedGradientEnd
        { get { return Color.FromArgb(51, 51, 52); } }

        public override Color MenuItemBorder
        { get { return Color.FromArgb(51, 51, 52); } }

        public override Color MenuItemPressedGradientBegin
        { get { return Color.FromArgb(51, 51, 52); } }

        public override Color MenuItemPressedGradientEnd
        { get { return Color.FromArgb(45, 45, 48); } }

        public override Color MenuStripGradientBegin
        { get { return Color.FromArgb(51, 51, 52); } }

        public override Color MenuStripGradientEnd
        { get { return Color.FromArgb(51, 51, 52); } }

        #endregion

        #region Separators

        public override Color SeparatorDark
        {
            get { return Color.FromArgb(51, 51, 55); }
        }

        public override Color SeparatorLight
        {
            get { return Color.FromArgb(51, 51, 55); }
        }

        #endregion

        #region ToolStrip

        public override Color ToolStripBorder
        {
            get { return Color.FromArgb(51, 51, 52); }
        }
        public override Color ToolStripContentPanelGradientBegin
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ToolStripContentPanelGradientEnd
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ToolStripDropDownBackground
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ToolStripGradientBegin
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ToolStripGradientEnd
        {
            get { return Color.FromArgb(45, 45, 48); }
        }
        public override Color ToolStripGradientMiddle
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ToolStripPanelGradientBegin
        {
            get { return Color.FromArgb(45, 45, 48); }
        }
        public override Color ToolStripPanelGradientEnd
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        #endregion

        #region ImageMargin

        public override Color ImageMarginGradientBegin
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ImageMarginGradientMiddle
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        public override Color ImageMarginGradientEnd
        {
            get { return Color.FromArgb(45, 45, 48); }
        }

        #endregion

        #region CheckBackground
        public override Color CheckBackground
        {
            get { return Color.FromArgb(73, 73, 73); }
        }
        #endregion
    }
}
