﻿namespace PawnPlus
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ThemeCombo = new System.Windows.Forms.ComboBox();
            this.GeneralPanel = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LanguageCombo = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.UserNameBox = new System.Windows.Forms.TextBox();
            this.OptionList = new System.Windows.Forms.ListBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CompilerPanel = new System.Windows.Forms.Panel();
            this.CompilingParametersText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CompilerNameText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.GeneralPanel.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.CompilerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ThemeCombo);
            this.groupBox1.Location = new System.Drawing.Point(229, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 49);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Theme";
            // 
            // ThemeCombo
            // 
            this.ThemeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ThemeCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ThemeCombo.FormattingEnabled = true;
            this.ThemeCombo.Location = new System.Drawing.Point(6, 17);
            this.ThemeCombo.Name = "ThemeCombo";
            this.ThemeCombo.Size = new System.Drawing.Size(162, 21);
            this.ThemeCombo.TabIndex = 0;
            // 
            // GeneralPanel
            // 
            this.GeneralPanel.Controls.Add(this.groupBox3);
            this.GeneralPanel.Controls.Add(this.groupBox2);
            this.GeneralPanel.Controls.Add(this.groupBox1);
            this.GeneralPanel.Location = new System.Drawing.Point(160, 13);
            this.GeneralPanel.Name = "GeneralPanel";
            this.GeneralPanel.Size = new System.Drawing.Size(406, 316);
            this.GeneralPanel.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LanguageCombo);
            this.groupBox3.Location = new System.Drawing.Point(3, 96);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(174, 49);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Language";
            // 
            // LanguageCombo
            // 
            this.LanguageCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LanguageCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LanguageCombo.FormattingEnabled = true;
            this.LanguageCombo.Location = new System.Drawing.Point(6, 17);
            this.LanguageCombo.Name = "LanguageCombo";
            this.LanguageCombo.Size = new System.Drawing.Size(162, 21);
            this.LanguageCombo.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.UserNameBox);
            this.groupBox2.Location = new System.Drawing.Point(3, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(174, 49);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "User name";
            // 
            // UserNameBox
            // 
            this.UserNameBox.Location = new System.Drawing.Point(6, 17);
            this.UserNameBox.Name = "UserNameBox";
            this.UserNameBox.Size = new System.Drawing.Size(162, 20);
            this.UserNameBox.TabIndex = 0;
            // 
            // OptionList
            // 
            this.OptionList.BackColor = System.Drawing.SystemColors.Control;
            this.OptionList.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionList.FormattingEnabled = true;
            this.OptionList.ItemHeight = 26;
            this.OptionList.Location = new System.Drawing.Point(13, 13);
            this.OptionList.Name = "OptionList";
            this.OptionList.Size = new System.Drawing.Size(120, 316);
            this.OptionList.TabIndex = 5;
            this.OptionList.SelectedIndexChanged += new System.EventHandler(this.OptionList_SelectedIndexChanged);
            // 
            // OkButton
            // 
            this.OkButton.FlatAppearance.BorderSize = 0;
            this.OkButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OkButton.Location = new System.Drawing.Point(169, 340);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(108, 23);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = false;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CompilerPanel
            // 
            this.CompilerPanel.Controls.Add(this.label2);
            this.CompilerPanel.Controls.Add(this.CompilerNameText);
            this.CompilerPanel.Controls.Add(this.label1);
            this.CompilerPanel.Controls.Add(this.CompilingParametersText);
            this.CompilerPanel.Location = new System.Drawing.Point(160, 13);
            this.CompilerPanel.Name = "CompilerPanel";
            this.CompilerPanel.Size = new System.Drawing.Size(406, 316);
            this.CompilerPanel.TabIndex = 8;
            // 
            // CompilingParametersText
            // 
            this.CompilingParametersText.Location = new System.Drawing.Point(39, 125);
            this.CompilingParametersText.Name = "CompilingParametersText";
            this.CompilingParametersText.Size = new System.Drawing.Size(347, 20);
            this.CompilingParametersText.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Compiling Parameters:";
            // 
            // CompilerNameText
            // 
            this.CompilerNameText.Location = new System.Drawing.Point(39, 49);
            this.CompilerNameText.Name = "CompilerNameText";
            this.CompilerNameText.Size = new System.Drawing.Size(347, 20);
            this.CompilerNameText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Compiler Name:";
            // 
            // CloseButton
            // 
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Location = new System.Drawing.Point(362, 340);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(108, 23);
            this.CloseButton.TabIndex = 9;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 375);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.OptionList);
            this.Controls.Add(this.CompilerPanel);
            this.Controls.Add(this.GeneralPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Options";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Options_Load);
            this.groupBox1.ResumeLayout(false);
            this.GeneralPanel.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.CompilerPanel.ResumeLayout(false);
            this.CompilerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel GeneralPanel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox UserNameBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Panel CompilerPanel;
        public System.Windows.Forms.ListBox OptionList;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox CompilerNameText;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox CompilingParametersText;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.ComboBox ThemeCombo;
        public System.Windows.Forms.ComboBox LanguageCombo;


    }
}