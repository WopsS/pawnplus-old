﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace PawnPlus
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        private void Options_Load(object sender, EventArgs e)
        {
            if (ConfigFiles.Theme == "Black")
            {
                this.BackColor = Color.FromArgb(45, 45, 48);
                this.ForeColor = Color.White;

                OptionList.BackColor = Color.FromArgb(45, 45, 48);
                OptionList.ForeColor = Color.White;

                groupBox1.BackColor = Color.FromArgb(45, 45, 48);
                groupBox1.ForeColor = Color.White;

                groupBox2.BackColor = Color.FromArgb(45, 45, 48);
                groupBox2.ForeColor = Color.White;
            }

            TranslateToLanguage();
            HideAllPanels();

            LanguageCombo.Items.Add("English");
            LanguageCombo.Items.Add("Spanish");
            LanguageCombo.Items.Add("Romanian");

            UserNameBox.Text = ConfigFiles.User;
            ThemeCombo.Text = ConfigFiles.Theme;
            LanguageCombo.Text = ConfigFiles.Language;

            CompilerNameText.Text = ConfigFiles.CompilerName;
            CompilingParametersText.Text = ConfigFiles.CompilerArgs;
        }

        private void OptionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HideAllPanels();

            if (OptionList.SelectedItem.ToString() != "")
            {
                if (OptionList.SelectedItem.ToString() == "General")
                    GeneralPanel.Show();

                if (OptionList.SelectedItem.ToString() == "Compiler")
                    CompilerPanel.Show();
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            ConfigFiles.User = UserNameBox.Text;
            ConfigFiles.Theme = ThemeCombo.Text;
            ConfigFiles.Language = LanguageCombo.Text;
            ConfigFiles.CompilerName = CompilerNameText.Text;
            ConfigFiles.CompilerArgs = CompilingParametersText.Text;

            ConfigFiles.SaveConfigFile();

            ConfigFiles.ThemeChanged = true;

            ConfigFiles.LanguageChanged = true;

            this.Close();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HideAllPanels()
        {
            GeneralPanel.Hide();
            CompilerPanel.Hide();
        }
        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "English")
            {
                OptionList.Items.Add("General");
                OptionList.Items.Add("Compiler");

                ThemeCombo.Items.Add("Black");
                ThemeCombo.Items.Add("White");

            }
            if (ConfigFiles.Language == "Romanian")
            {
                OptionList.Items.Add("General");
                OptionList.Items.Add("Compilator");

                ThemeCombo.Items.Add("Negru");
                ThemeCombo.Items.Add("Alb");

                groupBox1.Text = "Utilizator";
                groupBox2.Text = "Temă";
                groupBox3.Text = "Limbă";

                label1.Text = "Nume compilator:";
                label2.Text = "Parametri compilator:";

            }
            if (ConfigFiles.Language == "Spanish")
            {
                OptionList.Items.Add("General");
                OptionList.Items.Add("Compilador");

                ThemeCombo.Items.Add("Negro");
                ThemeCombo.Items.Add("Blanco");

                groupBox1.Text = "Nombre de usuario";
                groupBox2.Text = "Tema";
                groupBox3.Text = "Idioma";

                label1.Text = "Nombre del compilador:";
                label2.Text = "Parámetros de compilación:";

                OkButton.Text = "Guardar";
                CloseButton.Text = "Cerrar";
            }
        }
    }
}
