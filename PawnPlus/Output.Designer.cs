﻿namespace PawnPlus
{
    partial class Output
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // OutBox
            // 
            this.OutBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OutBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutBox.Font = new System.Drawing.Font("Courier New", 9F);
            this.OutBox.Location = new System.Drawing.Point(0, 0);
            this.OutBox.Name = "OutBox";
            this.OutBox.ReadOnly = true;
            this.OutBox.Size = new System.Drawing.Size(784, 562);
            this.OutBox.TabIndex = 0;
            this.OutBox.Text = "";
            this.OutBox.DoubleClick += new System.EventHandler(this.OutBox_DoubleClick);
            this.OutBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OutBox_MouseDown);
            // 
            // Output
            // 
            this.AutoHidePortion = 0.15D;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.OutBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Output";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockBottom;
            this.Text = "Output";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.RichTextBox OutBox;


    }
}