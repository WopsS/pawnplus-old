﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;

namespace PawnPlus
{
    public partial class UpdateAvailable : Form
    {
        Downloading downloading = new Downloading();

        public UpdateAvailable()
        {
            InitializeComponent();

        }

        private void Updater_Load(object sender, EventArgs e)
        {
            if (ConfigFiles.Theme == "Black")
            {
                this.BackColor = Color.FromArgb(45, 45, 48);
                this.ForeColor = Color.White;
                UpdateText.ForeColor = Color.White;
                CurrentVersionLabel.ForeColor = Color.White;
                UpdateText2.ForeColor = Color.White;
            }
            TranslateToLanguage();
        }

        private void YesButton_Click(object sender, EventArgs e)
        {
            downloading.ShowDialog();
        }

        private void NoButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romanian")
            {
                this.Text = "Actualizare disponibilă";
                YesButton.Text = "Da";
                NoButton.Text = "Nu";
            }

            if (ConfigFiles.Language == "Spanish")
            {
                this.Text = "Actualización disponible";
                YesButton.Text = "Sí";
            }
        }
    }
}
