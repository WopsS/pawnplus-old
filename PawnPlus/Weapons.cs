﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace PawnPlus
{
    public partial class Weapons : DockContent
    {
        string[] WeaponName = { "Unarmed", "Brass knuckles", "Golf club", "Nite stick", "Knife", "Baseball bat", "Shovel", "Pool cue", "Kantana", "Chainsaw", "Purple dildo", "Short vibrator", "Long vibrator"
                                  , "White dildo", "Flowers", "Cane", "Grenades", "Tear gas", "Molotov cocktail", "Vehicle missile", "Hydra flare", "Jetpack", "9mm Pistol", "Silenced pistol", "Desert eagle"
                                  , "Shotgun", "Sawn-off shotgun", "Combat shotgun", "Micro Uzi (Mac 10)", "Mp5", "Ak47", "M4", "Tec 9", "Country rifle", "Sniper rifle", "Rpg", "Heat seeking rocket", "Flame-thrower"
                                  , "Mini gun", "Satchel charges", "Detonator", "Spray can", "Fire extinguisher", "Camera", "Night vision", "Thermal goggles", "Parachute"
                              };

        public Weapons()
        {
            InitializeComponent();
        }

        private void Weapons_Load(object sender, EventArgs e)
        {
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            TranslateToLanguage();
            SetColumnSize(WeaponListView);
            WeaponListView.Visible = false;
            WeaponsWorker.RunWorkerAsync();
        }

        private void Weapons_Resize(object sender, EventArgs e)
        {
            try
            {
                SetColumnSize((ListView)sender);
            }
            catch { }
        }

        private void WeaponsWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            imageList.ImageSize = new Size(100, 100);

            for (int i = 0; i <= 46; i++)
            {
                imageList.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory + "\\Weapons", "Weapon_" + i + ".png")));
                InvokeListView.ControlInvike(WeaponListView, () => WeaponListView.Items.Add(WeaponName[i] + ", ID " + i, i));
            }


            InvokeListView.ControlInvike(WeaponListView, () => WeaponListView.LargeImageList = imageList);
            InvokeListView.ControlInvike(WeaponListView, () => WeaponListView.View = View.LargeIcon);
        }

        private void SetColumnSize(ListView listview)
        {
            listview.Columns[listview.Columns.Count - 1].Width = -1;
        }

        private void WeaponsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadingLabel.Visible = false;
            InvokeListView.ControlInvike(WeaponListView, () => WeaponListView.Visible = true);
        }
        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romana")
            {
                this.Text = "Arme";
                LoadingLabel.Text = "Încărcare";
            }

            if (ConfigFiles.Language == "Spanish")
            {
                this.Text = "Armas";
            }
        }
    }
}
