﻿namespace PawnPlus
{
    partial class Downloading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Downloading));
            this.DonwloadProcessBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.DownloadPercent = new System.Windows.Forms.Label();
            this.MBDownloaded = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DonwloadProcessBar
            // 
            this.DonwloadProcessBar.BackColor = System.Drawing.SystemColors.Control;
            this.DonwloadProcessBar.Location = new System.Drawing.Point(12, 50);
            this.DonwloadProcessBar.Name = "DonwloadProcessBar";
            this.DonwloadProcessBar.Size = new System.Drawing.Size(339, 43);
            this.DonwloadProcessBar.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("CordiaUPC", 20F);
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Downloading update...";
            // 
            // DownloadPercent
            // 
            this.DownloadPercent.AutoSize = true;
            this.DownloadPercent.Font = new System.Drawing.Font("CordiaUPC", 15F);
            this.DownloadPercent.Location = new System.Drawing.Point(9, 106);
            this.DownloadPercent.Name = "DownloadPercent";
            this.DownloadPercent.Size = new System.Drawing.Size(31, 28);
            this.DownloadPercent.TabIndex = 2;
            this.DownloadPercent.Text = "0%";
            // 
            // MBDownloaded
            // 
            this.MBDownloaded.AutoSize = true;
            this.MBDownloaded.Font = new System.Drawing.Font("CordiaUPC", 15F);
            this.MBDownloaded.Location = new System.Drawing.Point(120, 106);
            this.MBDownloaded.Name = "MBDownloaded";
            this.MBDownloaded.Size = new System.Drawing.Size(106, 28);
            this.MBDownloaded.TabIndex = 3;
            this.MBDownloaded.Text = "0 MB\'s / 0 MB\'s";
            // 
            // Downloading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 149);
            this.ControlBox = false;
            this.Controls.Add(this.MBDownloaded);
            this.Controls.Add(this.DownloadPercent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DonwloadProcessBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Downloading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Downloading";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Downloading_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar DonwloadProcessBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label DownloadPercent;
        private System.Windows.Forms.Label MBDownloaded;
    }
}