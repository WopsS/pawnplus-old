﻿namespace PawnPlus
{
    partial class Skins
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Skins));
            this.SkinListView = new System.Windows.Forms.ListView();
            this.Column = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.SkinsWorker = new System.ComponentModel.BackgroundWorker();
            this.LoadingLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SkinListView
            // 
            this.SkinListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.SkinListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SkinListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Column});
            this.SkinListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SkinListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.SkinListView.Location = new System.Drawing.Point(0, 0);
            this.SkinListView.Name = "SkinListView";
            this.SkinListView.Size = new System.Drawing.Size(325, 435);
            this.SkinListView.TabIndex = 0;
            this.SkinListView.UseCompatibleStateImageBehavior = false;
            this.SkinListView.View = System.Windows.Forms.View.Details;
            this.SkinListView.Resize += new System.EventHandler(this.SkinListView_Resize);
            // 
            // Column
            // 
            this.Column.Text = "Column";
            this.Column.Width = 303;
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(55, 100);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // SkinsWorker
            // 
            this.SkinsWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SkinsWorker_DoWork);
            this.SkinsWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SkinsWorker_RunWorkerCompleted);
            // 
            // LoadingLabel
            // 
            this.LoadingLabel.BackColor = System.Drawing.Color.Transparent;
            this.LoadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadingLabel.Font = new System.Drawing.Font("CordiaUPC", 50F);
            this.LoadingLabel.Location = new System.Drawing.Point(0, 0);
            this.LoadingLabel.Name = "LoadingLabel";
            this.LoadingLabel.Size = new System.Drawing.Size(325, 435);
            this.LoadingLabel.TabIndex = 1;
            this.LoadingLabel.Text = "Loading...";
            this.LoadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Skins
            // 
            this.AutoHidePortion = 0.15D;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 435);
            this.Controls.Add(this.LoadingLabel);
            this.Controls.Add(this.SkinListView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Skins";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Skins";
            this.Load += new System.EventHandler(this.Skins_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader Column;
        public System.Windows.Forms.ListView SkinListView;
        private System.Windows.Forms.ImageList imageList;
        private System.ComponentModel.BackgroundWorker SkinsWorker;
        private System.Windows.Forms.Label LoadingLabel;



    }
}