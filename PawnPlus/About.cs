﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace PawnPlus
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void About_Load(object sender, EventArgs e)
        {
            if (ConfigFiles.Theme == "Black")
            {
                this.BackColor = Color.FromArgb(45, 45, 48);
                this.ForeColor = Color.White;

                linkLabel1.LinkColor = Color.FromArgb(86, 156, 214);
            }

            TranslateToLanguage();

            string YearNow = DateTime.Now.ToString("yyyy");
            string NextYear = DateTime.Now.AddYears(2).ToString("yyyy");

            Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

            label4.Text = "Copyright ©" + YearNow + " - " + NextYear + " PawnPlus " + CurrentVersion.Major + "." + CurrentVersion.Minor + "." + CurrentVersion.Build + ", IDE.";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://icons8.com/");   
        }

        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romanian")
            {
                this.Text = "Despre";
                label1.Text = "Va mulţumesc că utilizaţi PawnPlus.";
                label2.Text = "PawnPlus este creat de Dima Octavian.";
                label3.Text = "PawnPlus a fost create pentru a face muncă de scripter mai uşoară.\r\n\r\nAcesta este că şi PAWNO dar mai îmbunătăţit.\r\n\r\nCateva dintre acestea sunt: Autocompletare, taburi şi un design bun.\r\n\r\nPawnPlus va primi update-uri în continuare.";
                label5.Text = "Iconiţe create de:";
            }
            if (ConfigFiles.Language == "Spanish")
            {
                this.Text = "Acerca de";
                label1.Text = "Gracias por usar PawnPlus";
                label2.Text = "PawnPlus fue creado por Dima Octavian.\r\nTraducido por Edugta.";
                label3.Text = "PawnPlus fue creado para ayudar a los scripters.\r\n\r\nEs como PAWNO, sólo que con características nuevas.\r\n\r\nLas cuales son: Autocompletacion, tabs y un buen diseño.\r\n\r\nPawnPlus recibirá actualizaciones con muchas mejoras.";
                label5.Text = "Iconos creados por:";
            }
        }
    }
}
