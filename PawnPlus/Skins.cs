﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.Net;

namespace PawnPlus
{
    public partial class Skins : DockContent
    {

        public Skins()
        {
            InitializeComponent();
        }

        private void Skins_Load(object sender, EventArgs e)
        {
            TranslateToLanguage();
            SetColumnSize(SkinListView);
            SkinListView.Visible = false;
            SkinsWorker.RunWorkerAsync();

        }

        private void SkinListView_Resize(object sender, EventArgs e)
        {
            try
            {
                SetColumnSize((ListView)sender);
            }
            catch { }
        }

        private void SetColumnSize(ListView listview)
        {
            listview.Columns[listview.Columns.Count - 1].Width = -1;
        }

        private void SkinsWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            imageList.ImageSize = new Size(120, 240);

            for (int i = 0; i <= 299; i++)
            {
                imageList.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory + "\\Skins", "Skin_" + i + ".png")));
                InvokeListView.ControlInvike(SkinListView, () => SkinListView.Items.Add("Skin " + i, i));
            }


            InvokeListView.ControlInvike(SkinListView, () => SkinListView.LargeImageList = imageList);
            InvokeListView.ControlInvike(SkinListView, () => SkinListView.View = View.LargeIcon);

        }

        private void SkinsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadingLabel.Visible = false;
            InvokeListView.ControlInvike(SkinListView, () => SkinListView.Visible = true);
        }

        public void TranslateToLanguage()
        {
            if (ConfigFiles.Language == "Romana")
            {
                this.Text = "Skin-uri";
                LoadingLabel.Text = "Încărcare";
            }
        }
    }
}
